"""
Diese Datei ist Bestandteil des STTV TT-Tools, 2020 entwickelt von Manuel Haake. Das STTV TT-Tool ist freie Software unter den Bedingungen der GNU General Public License Version 3 (GPL3). Die Haftung ist ausgeschlossen.
Diesem Programm sollte die Datei LICENSE beiliegen, welche die Lizenzbedingungen enthält. Sollte dies nicht der Fall sein, können Sie die Lizenz bei der Free Software Foundation (www.fsf.org) anfordern: Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
Alternativ kann die Lizens unter https://www.gnu.org/licenses/gpl-3.0.html aufgerufen werden

Manuel Haake | 09/2020
"""

import sys
import os
import tt_funktionen_gui
import submenus
import tkinter as tk
from tkinter import ttk as ttk
from tkinter import messagebox
from sys import platform
from queue import Queue
from threading import Thread
from time import sleep


# TODO Check Stadtliga Chemnitz Teamsübersicht mit (RR) Rückrundenteams...
# TODO Check Effizienz mulithread (Leutzscher Füchse vs Bautzen -> 34sec ; Sebnitz 1 vs Königstein 2 -> 9,7sec)

def read_and_insert_settings():
    try:
        progressbar = ttk.Progressbar(frame_bottom, orient="horizontal", length=500, mode="determinate")
        progressbar.place(relx = 0.5, rely = 0.4, anchor = tk.CENTER)
        progressbar.start()
        progressbar['value'] += 5
        root.update_idletasks()

        with open(settingsfile, "r") as f:
            settings = f.readlines()
        Verband2 = settings[0].replace("\n", "")
        Liga2 = settings[1].replace("\n", "")

        progressbar['value'] += 20
        root.update_idletasks()
        show_dropdown_verbandsauswahl()
        progressbar['value'] += 20
        root.update_idletasks()
        verbandsauswahl.set(Verband2)
        #show_dropdown_liga()
        progressbar['value'] += 20
        root.update_idletasks()
        ligaauswahl.set(Liga2)
        #getliganummer()
        progressbar.stop()
        progressbar.place_forget()

        Anweisung.set("Bitte Mannschaften auswählen.")

    except:
        verbandsauswahl.set("Waehle einen Verband aus...")
        ligaauswahl.set("Waehle eine Liga aus...")
        show_dropdown_verbandsauswahl()

def show_dropdown_verbandsauswahl(*args):
    global verbandsauswahl, verbandslink, verbandsname

    verbandslink, verbandsname = tt_funktionen_gui.waehle_Verband_Links(sttv_mainpage)
    
    dropdown_verband.config(state=tk.NORMAL)
    dropdown_liga.config(state=tk.DISABLED)
    dropdown_team1.config(state=tk.DISABLED)
    dropdown_team2.config(state=tk.DISABLED)
    
    dropdown_team1spieler1.config(state=tk.DISABLED)
    dropdown_team1spieler2.config(state=tk.DISABLED)
    dropdown_team1spieler3.config(state=tk.DISABLED)
    dropdown_team2spieler1.config(state=tk.DISABLED)
    dropdown_team2spieler2.config(state=tk.DISABLED)
    dropdown_team2spieler3.config(state=tk.DISABLED)
    dropdown_team1spieler4.config(state=tk.DISABLED)
    dropdown_team2spieler4.config(state=tk.DISABLED)
    dropdown_team1spieler5.config(state=tk.DISABLED)
    dropdown_team2spieler5.config(state=tk.DISABLED)
    dropdown_team1spieler6.config(state=tk.DISABLED)
    dropdown_team2spieler6.config(state=tk.DISABLED)

    update_dropdown_verband(verbandsname)


def show_dropdown_liga(*args):
    global ligaauswahl, ligalink, verbandslink_gui, liganame, run

    if not run == 0 or not os.path.isfile(settingsfile):
        Anweisung.set("Liga auswählen")

    run +=1

    team1auswahl.set("Waehle die Heimmannschaft aus...")
    team2auswahl.set("Waehle die Gastmannschaft aus...")
    ligaauswahl.set("Waehle eine Liga aus...")

    reset_entrys()

    Verband = verbandsauswahl.get()
    for j in range(0, len(verbandslink)):
        if Verband in verbandsname[j]:
            verbandsnummer = j
            break
    verbandslink_gui = verbandslink[verbandsnummer]
    ligalink, liganame = tt_funktionen_gui.waehle_Liga_links(verbandslink_gui)

    dropdown_verband.config(state=tk.NORMAL)
    dropdown_liga.config(state=tk.NORMAL)
    dropdown_team1.config(state=tk.DISABLED)
    dropdown_team2.config(state=tk.DISABLED)

    dropdown_team1spieler1.config(state=tk.DISABLED)
    dropdown_team1spieler2.config(state=tk.DISABLED)
    dropdown_team1spieler3.config(state=tk.DISABLED)
    dropdown_team2spieler1.config(state=tk.DISABLED)
    dropdown_team2spieler2.config(state=tk.DISABLED)
    dropdown_team2spieler3.config(state=tk.DISABLED)
    dropdown_team1spieler4.config(state=tk.DISABLED)
    dropdown_team2spieler4.config(state=tk.DISABLED)
    dropdown_team1spieler5.config(state=tk.DISABLED)
    dropdown_team2spieler5.config(state=tk.DISABLED)
    dropdown_team1spieler6.config(state=tk.DISABLED)
    dropdown_team2spieler6.config(state=tk.DISABLED)

    update_dropdown_liga(liganame)


def getliganummer(*args):
    global liganummer

    # setting-file schreiben
    with open(settingsfile, "w") as f:
        f.write(verbandsauswahl.get())
        f.write("\n")
        f.write(ligaauswahl.get())

    # Auswahlfenster zurücksetzen (notwenidg, falls aus späteren Menüverlauf die Liga geändert wird.
    team1auswahl.set("Waehle die Heimmannschaft aus...")
    team2auswahl.set("Waehle die Gastmannschaft aus...")

    reset_entrys()

    # Spielsystem auslesen
    if "Waehle" not in ligaauswahl.get():
        Liga = ligaauswahl.get()
        for j in range(0, len(ligalink)):
            if Liga in liganame[j]:
                liganummer = j
                break
    else:
        return
    getspielsystem()


def getspielsystem(*args):
    global spielsystem
    spielsystem = tt_funktionen_gui.pruefe_ligasystem(ligalink[liganummer], verbandslink_gui)
    
    # Wenn Spielsystem keines der unterstützten ist, Fehler ausgeben und dann reseten
    if spielsystem != 3 and spielsystem != 4 and spielsystem != 6:
        messagebox.showinfo(title="Achtung", message="Die ausgewählte Liga hat ein Spielsystem, welches bisher nicht unterstützt wird.\nZulässig sind:\n\n3er Swaythling\n4er Werner Scheffler\n6er Paarkreuz\n\n Das Programm wird neu gestartet.")
        restart_program()

    pickteam1()


def pickteam1(*args):
    # Mannschaft 1 auslesen
    global mannschaftslink1, mannschaftsname1, team1auswahl
    mannschaftslink1, mannschaftsname1 = tt_funktionen_gui.waehle_mannschafts_links(ligalink[liganummer], verbandslink_gui)

    Anweisung.set("Bitte Mannschaften auswählen")

    dropdown_verband.config(state=tk.NORMAL)
    dropdown_liga.config(state=tk.NORMAL)
    dropdown_team1.config(state=tk.NORMAL)
    dropdown_team2.config(state=tk.DISABLED)
    
    dropdown_team1spieler1.config(state=tk.DISABLED)
    dropdown_team1spieler2.config(state=tk.DISABLED)
    dropdown_team1spieler3.config(state=tk.DISABLED)
    dropdown_team2spieler1.config(state=tk.DISABLED)
    dropdown_team2spieler2.config(state=tk.DISABLED)
    dropdown_team2spieler3.config(state=tk.DISABLED)
    dropdown_team1spieler4.config(state=tk.DISABLED)
    dropdown_team2spieler4.config(state=tk.DISABLED)
    dropdown_team1spieler5.config(state=tk.DISABLED)
    dropdown_team2spieler5.config(state=tk.DISABLED)
    dropdown_team1spieler6.config(state=tk.DISABLED)
    dropdown_team2spieler6.config(state=tk.DISABLED)

    update_dropdown_team1(mannschaftsname1)

       
       
def pickteam2(*args):
    # Mannschaft 2 auslesen
    global mannschaftslink2, mannschaftsname2, team2auswahl, team1_pick_link, team1_pick_name

    reset_entrys()

    if "Waehle" not in team1auswahl.get():
        team1_pick_name = team1auswahl.get()
        for j in range(0, len(mannschaftsname1)):
            if team1_pick_name in mannschaftsname1[j]:
                team1_pick_number = j
        team1_pick_link = mannschaftslink1[team1_pick_number]

        mannschaftslink2 = mannschaftslink1
        mannschaftsname2 = mannschaftsname1
        dropdown_team2.config(state=tk.NORMAL)

        dropdown_team1spieler1.config(state=tk.DISABLED)
        dropdown_team1spieler2.config(state=tk.DISABLED)
        dropdown_team1spieler3.config(state=tk.DISABLED)
        dropdown_team2spieler1.config(state=tk.DISABLED)
        dropdown_team2spieler2.config(state=tk.DISABLED)
        dropdown_team2spieler3.config(state=tk.DISABLED)
        dropdown_team1spieler4.config(state=tk.DISABLED)
        dropdown_team2spieler4.config(state=tk.DISABLED)
        dropdown_team1spieler5.config(state=tk.DISABLED)
        dropdown_team2spieler5.config(state=tk.DISABLED)
        dropdown_team1spieler6.config(state=tk.DISABLED)
        dropdown_team2spieler6.config(state=tk.DISABLED)

        update_dropdown_team2(mannschaftsname2)
        team2auswahl.set("Waehle die Gastmannschaft aus...")

    else:
        return

def reset_entrys(*args):
    team1spieler1.set("Pos. 1")
    team1spieler2.set("Pos. 2")
    team1spieler3.set("Pos. 3")
    team1spieler4.set("Pos. 4")
    team1spieler5.set("Pos. 5")
    team1spieler6.set("Pos. 6")

    team2spieler1.set("Pos. 1")
    team2spieler2.set("Pos. 2")
    team2spieler3.set("Pos. 3")
    team2spieler4.set("Pos. 4")
    team2spieler5.set("Pos. 5")
    team2spieler6.set("Pos. 6")

    LABEL_LPZ11.set("")
    LABEL_LPZ12.set("")
    LABEL_LPZ13.set("")
    LABEL_LPZ14.set("")
    LABEL_LPZ15.set("")
    LABEL_LPZ16.set("")

    LABEL_LPZ21.set("")
    LABEL_LPZ22.set("")
    LABEL_LPZ23.set("")
    LABEL_LPZ24.set("")
    LABEL_LPZ25.set("")
    LABEL_LPZ26.set("")

def read_aufstellung_team1(*args):
    global team1aufstellung, team2_pick_link, team2_pick_name
    
    if "Waehle" not in team2auswahl.get():
        team2_pick_name = team2auswahl.get()
        for j in range(0, len(mannschaftsname2)):
            if team2_pick_name in mannschaftsname2[j]:
                team2_pick_number = j
        team2_pick_link = mannschaftslink2[team2_pick_number]

        if team1auswahl.get() == team2auswahl.get():
            messagebox.showinfo(title="Achtung", message="Sie haben zwei Mal die gleiche Mannschaft ausgewaehlt. Bitte korrigieren Sie ihre Auswahl.")
            return
            #pickteam2

        dropdown_team1spieler1.config(state=tk.DISABLED)
        dropdown_team1spieler2.config(state=tk.DISABLED)
        dropdown_team1spieler3.config(state=tk.DISABLED)
        dropdown_team2spieler1.config(state=tk.DISABLED)
        dropdown_team2spieler2.config(state=tk.DISABLED)
        dropdown_team2spieler3.config(state=tk.DISABLED)
        dropdown_team1spieler4.config(state=tk.DISABLED)
        dropdown_team2spieler4.config(state=tk.DISABLED)
        dropdown_team1spieler5.config(state=tk.DISABLED)
        dropdown_team2spieler5.config(state=tk.DISABLED)
        dropdown_team1spieler6.config(state=tk.DISABLED)
        dropdown_team2spieler6.config(state=tk.DISABLED)

        read_aufstellung_team2()
    else:
        return
    
def read_aufstellung_team2(*args):
    global team2aufstellung

    suche_moegliche_spieler()
    
    
def update_dropdown_verband(Verbaende):
    dropdown_verband['menu'].delete(0, 'end')
    for auswahl in Verbaende:
        dropdown_verband['menu'].add_command(label=auswahl, command=tk._setit(verbandsauswahl, auswahl))


def update_dropdown_liga(Ligen):
    dropdown_liga['menu'].delete(0, 'end')
    for auswahl in Ligen:
        dropdown_liga['menu'].add_command(label=auswahl, command=tk._setit(ligaauswahl, auswahl))


def update_dropdown_team1(Teams):
    dropdown_team1['menu'].delete(0, 'end')
    for auswahl in Teams:
        dropdown_team1['menu'].add_command(label=auswahl, command=tk._setit(team1auswahl, auswahl))


def update_dropdown_team2(Teams):
    dropdown_team2['menu'].delete(0, 'end')
    for auswahl in Teams:
        dropdown_team2['menu'].add_command(label=auswahl, command=tk._setit(team2auswahl, auswahl))


def update_dropdown_spieler_heim(Optionen):
    dropdown_team1spieler1['menu'].delete(0, 'end')
    for auswahl in Optionen:
        dropdown_team1spieler1['menu'].add_command(label=auswahl, command=tk._setit(team1spieler1, auswahl))
        
    dropdown_team1spieler2['menu'].delete(0, 'end')
    for auswahl in Optionen:
        dropdown_team1spieler2['menu'].add_command(label=auswahl, command=tk._setit(team1spieler2, auswahl))

    dropdown_team1spieler3['menu'].delete(0, 'end')
    for auswahl in Optionen:
        dropdown_team1spieler3['menu'].add_command(label=auswahl, command=tk._setit(team1spieler3, auswahl))
    
    if spielsystem == 4 or spielsystem == 6:
        dropdown_team1spieler4['menu'].delete(0, 'end')
        for auswahl in Optionen:
            dropdown_team1spieler4['menu'].add_command(label=auswahl, command=tk._setit(team1spieler4, auswahl))
        
    if spielsystem == 6:
        dropdown_team1spieler5['menu'].delete(0, 'end')
        for auswahl in Optionen:
            dropdown_team1spieler5['menu'].add_command(label=auswahl, command=tk._setit(team1spieler5, auswahl))

        dropdown_team1spieler6['menu'].delete(0, 'end')
        for auswahl in Optionen:
            dropdown_team1spieler6['menu'].add_command(label=auswahl, command=tk._setit(team1spieler6, auswahl))


def update_dropdown_spieler_ausw(Optionen):
    dropdown_team2spieler1['menu'].delete(0, 'end')
    for auswahl in Optionen:
        dropdown_team2spieler1['menu'].add_command(label=auswahl, command=tk._setit(team2spieler1, auswahl))
        
    dropdown_team2spieler2['menu'].delete(0, 'end')
    for auswahl in Optionen:
        dropdown_team2spieler2['menu'].add_command(label=auswahl, command=tk._setit(team2spieler2, auswahl))

    dropdown_team2spieler3['menu'].delete(0, 'end')
    for auswahl in Optionen:
        dropdown_team2spieler3['menu'].add_command(label=auswahl, command=tk._setit(team2spieler3, auswahl))
    
    if spielsystem == 4 or spielsystem == 6:
        dropdown_team2spieler4['menu'].delete(0, 'end')
        for auswahl in Optionen:
            dropdown_team2spieler4['menu'].add_command(label=auswahl, command=tk._setit(team2spieler4, auswahl))
        
    if spielsystem == 6:
        dropdown_team2spieler5['menu'].delete(0, 'end')
        for auswahl in Optionen:
            dropdown_team2spieler5['menu'].add_command(label=auswahl, command=tk._setit(team2spieler5, auswahl))

        dropdown_team2spieler6['menu'].delete(0, 'end')
        for auswahl in Optionen:
            dropdown_team2spieler6['menu'].add_command(label=auswahl, command=tk._setit(team2spieler6, auswahl))


def write_lpz(*args):
    # schreibt die aktuellen LPZ-Zahlen rechts neben die Spielernamen in die Oberfläche
    
    #Team1
    if moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler1.get(),1)-1] == 1:
        LABEL_LPZ11.set("[" + str(searchlpz(team1spieler1.get(),1)) + "]  ")
    elif moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler1.get(),1)-1] == 2:
        LABEL_LPZ11.set(str(searchlpz(team1spieler1.get(),1)) + "*  ")
    else:
        LABEL_LPZ11.set(str(searchlpz(team1spieler1.get(),1))+"  ")
    
    if moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler2.get(),1)-1] == 1:
        LABEL_LPZ12.set("[" + str(searchlpz(team1spieler2.get(),1)) + "]  ")
    elif moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler2.get(),1)-1] == 2:
        LABEL_LPZ12.set(str(searchlpz(team1spieler2.get(),1)) + "*  ")
    else:
        LABEL_LPZ12.set(str(searchlpz(team1spieler2.get(),1))+"  ")
        
    if moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler3.get(),1)-1] == 1:
        LABEL_LPZ13.set("[" + str(searchlpz(team1spieler3.get(),1)) + "]  ")
    elif moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler3.get(),1)-1] == 2:
        LABEL_LPZ13.set(str(searchlpz(team1spieler3.get(),1)) + "*  ")
    else:
        LABEL_LPZ13.set(str(searchlpz(team1spieler3.get(),1))+"  ")
        
    if spielsystem == 4 or spielsystem == 6:
        if moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler4.get(),1)-1] == 1:
            LABEL_LPZ14.set("[" + str(searchlpz(team1spieler4.get(),1)) + "]  ")
        elif moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler4.get(),1)-1] == 2:
            LABEL_LPZ14.set(str(searchlpz(team1spieler4.get(),1)) + "*  ")
        else:
            LABEL_LPZ14.set(str(searchlpz(team1spieler4.get(),1))+"  ")
        
    if spielsystem == 6:
        if moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler5.get(),1)-1] == 1:
            LABEL_LPZ15.set("[" + str(searchlpz(team1spieler5.get(),1)) + "]  ")
        elif moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler5.get(),1)-1] == 2:
            LABEL_LPZ15.set(str(searchlpz(team1spieler5.get(),1)) + "*  ")
        else:
            LABEL_LPZ15.set(str(searchlpz(team1spieler5.get(),1))+"  ")
            
        if moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler6.get(),1)-1] == 1:
            LABEL_LPZ16.set("[" + str(searchlpz(team1spieler6.get(),1)) + "]  ")
        elif moegliche_spielerinaktivitaet1 [getplayernumber(team1spieler6.get(),1)-1] == 2:
            LABEL_LPZ16.set(str(searchlpz(team1spieler6.get(),1)) + "*  ")
        else:
            LABEL_LPZ16.set(str(searchlpz(team1spieler6.get(),1))+"  ")

    #Team2
    if moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler1.get(),2)-1] == 1:
        LABEL_LPZ21.set("[" + str(searchlpz(team2spieler1.get(),2)) + "]  ")
    elif moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler1.get(),2)-1] == 2:
        LABEL_LPZ21.set(str(searchlpz(team2spieler1.get(),2)) + "*  ")
    else:
        LABEL_LPZ21.set(str(searchlpz(team2spieler1.get(),2))+"  ")
    
    if moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler2.get(),2)-1] == 1:
        LABEL_LPZ22.set("[" + str(searchlpz(team2spieler2.get(),2)) + "]  ")
    elif moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler2.get(),2)-1] == 2:
        LABEL_LPZ22.set(str(searchlpz(team2spieler2.get(),2)) + "*  ")
    else:
        LABEL_LPZ22.set(str(searchlpz(team2spieler2.get(),2))+"  ")
        
    if moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler3.get(),2)-1] == 1:
        LABEL_LPZ23.set("[" + str(searchlpz(team2spieler3.get(),2)) + "]  ")
    elif moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler3.get(),2)-1] == 2:
        LABEL_LPZ23.set(str(searchlpz(team2spieler3.get(),2)) + "*  ")
    else:
        LABEL_LPZ23.set(str(searchlpz(team2spieler3.get(),2))+"  ")
        
    if spielsystem == 4 or spielsystem == 6:
        if moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler4.get(),2)-1] == 1:
            LABEL_LPZ24.set("[" + str(searchlpz(team2spieler4.get(),2)) + "]  ")
        elif moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler4.get(),2)-1] == 2:
            LABEL_LPZ24.set(str(searchlpz(team2spieler4.get(),2)) + "*  ")
        else:
            LABEL_LPZ24.set(str(searchlpz(team2spieler4.get(),2))+"  ")
        
    if spielsystem == 6:
        if moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler5.get(),2)-1] == 1:
            LABEL_LPZ25.set("[" + str(searchlpz(team2spieler5.get(),2)) + "]  ")
        elif moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler5.get(),2)-1] == 2:
            LABEL_LPZ25.set(str(searchlpz(team2spieler5.get(),2)) + "*  ")
        else:
            LABEL_LPZ25.set(str(searchlpz(team2spieler5.get(),2))+"  ")
            
        if moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler6.get(),2)-1] == 1:
            LABEL_LPZ26.set("[" + str(searchlpz(team2spieler6.get(),2)) + "]  ")
        elif moegliche_spielerinaktivitaet2 [getplayernumber(team2spieler6.get(),2)-1] == 2:
            LABEL_LPZ26.set(str(searchlpz(team2spieler6.get(),2)) + "*  ")
        else:
            LABEL_LPZ26.set(str(searchlpz(team2spieler6.get(),2))+"  ")


def searchlpz(playername,team):
    lpz = ""
    if team == 1:
        for j in range(0, len(moegliche_spielername1)):
            if playername in moegliche_spielername1[j]:
                lpz = moegliche_spielerlpz1[j]
    
    elif team == 2:
        for j in range(0, len(moegliche_spielername2)):
            if playername in moegliche_spielername2[j]:
                lpz = moegliche_spielerlpz2[j]
    
    return lpz


def getplayernumber(playername,team):
    playernumber = -1
    if team == 1:
        for j in range(0, len(moegliche_spielername1)):
            if playername in moegliche_spielername1[j]:
                playernumber = moegliche_spielernummer1[j]
    
    elif team == 2:
        for j in range(0, len(moegliche_spielername2)):
            if playername in moegliche_spielername2[j]:
                playernumber = moegliche_spielernummer2[j]
    
    return playernumber


def progress(progressbar, thread1, thread2, queue):
    # checks whether thread is alive #
    if "Bezirk" in verbandsauswahl.get():
        step = 10
    elif "Sächsischer Tischtennis" in verbandsauswahl.get():
        step = 5
    else:
        step = 15

    while thread1.is_alive() or thread2.is_alive():
        sleep(1.25)
        progressbar['value'] += step
        root.update_idletasks()
        pass


def suche_moegliche_spieler(*args):
    global moegliche_spielername1, moegliche_spielername2, moegliche_spielerlpz1, moegliche_spielerlpz2, moegliche_spielerbemerkung1, moegliche_spielerbemerkung2, moegliche_spielernummer1, moegliche_spielernummer2, moegliche_spielerinaktivitaet1, moegliche_spielerinaktivitaet2
    global Checkage_U16_11, Checkage_U16_12, Checkage_U16_13, Checkage_U16_14, Checkage_U16_15, Checkage_U16_16
    global Checkage_U21_11, Checkage_U21_12, Checkage_U21_13, Checkage_U21_14, Checkage_U21_15, Checkage_U21_16
    global Checkage_U16_21, Checkage_U16_22, Checkage_U16_23, Checkage_U16_24, Checkage_U16_25, Checkage_U16_26
    global Checkage_U21_21, Checkage_U21_22, Checkage_U21_23, Checkage_U21_24, Checkage_U21_25, Checkage_U21_26
    
    global LABEL_LPZ11,LABEL_LPZ12 ,LABEL_LPZ13, LABEL_LPZ14, LABEL_LPZ15, LABEL_LPZ16
    global LABEL_LPZ21,LABEL_LPZ22 ,LABEL_LPZ23, LABEL_LPZ24, LABEL_LPZ25, LABEL_LPZ26

    # Progressbar anzeigen lassen für dauer der Zeit, die Spieler ausgelesen werden sowie Hinweis geben, was im Hintergrund passiert
    Anweisung.set("Suche nach möglichen Spieler/-innen für beide Teams")

    progressbar = ttk.Progressbar(frame_bottom, orient="horizontal", length=500, mode="indeterminate")
    progressbar.place(relx = 0.5, rely = 0.4, anchor = tk.CENTER)
    progressbar.start()

    out_queue1 = Queue()
    out_queue2 = Queue()
    out_queue3 = Queue()
    out_queue4 = Queue()
    queue = Queue()

    # Versuche, mögliche Stammspieler auslesen zu können
    try:
        #spielernummer1, spielername1, spielerlpz1, spielerbemerkung1, link_vereinsuebersicht1, spielerinaktivitaet1 = tt_funktionen_gui.suche_stammspieler(team1_pick_link, verbandslink_gui)
        #spielernummer2, spielername2, spielerlpz2, spielerbemerkung2, link_vereinsuebersicht2, spielerinaktivitaet2 = tt_funktionen_gui.suche_stammspieler(team2_pick_link, verbandslink_gui)
        t1 = Thread(target=tt_funktionen_gui.suche_stammspieler, args=(team1_pick_link, verbandslink_gui, out_queue1))
        t2 = Thread(target=tt_funktionen_gui.suche_stammspieler, args=(team2_pick_link, verbandslink_gui, out_queue2))
        t1.start()
        t2.start()
        progress(progressbar, t1, t2, queue)
        t1.join()
        t2.join()

        spielernummer1 = out_queue1.get()
        spielername1 = out_queue1.get()
        spielerlpz1 = out_queue1.get()
        spielerbemerkung1 = out_queue1.get()
        link_vereinsuebersicht1 = out_queue1.get()
        spielerinaktivitaet1 = out_queue1.get()

        spielernummer2 = out_queue2.get()
        spielername2 = out_queue2.get()
        spielerlpz2 = out_queue2.get()
        spielerbemerkung2 = out_queue2.get()
        link_vereinsuebersicht2 = out_queue2.get()
        spielerinaktivitaet2 = out_queue2.get()

    except:
        tk.messagebox.showinfo(title="Achtung", message="Die Aufstellungen der Mannschaften konnten nicht ausgelsesen werden.\n\n Dies ist notwendig, um möglichen Spieler/-innen für die Mannschaft herausfinden zu können. \n Die wahrscheinliste Ursache für den Fehler ist, dass noch keine Aufstellung verfügbar ist. \n \n Versuche es erneut, wenn die Aufstellungen freigegeben und online sind.")
        exit()

    if spielernummer1 == "" or spielernummer2 == "":
        tk.messagebox.showinfo(title="Achtung", message="Es konnten nicht alle Aufstellungen aller Mannschaften des Vereins ausgelsesen werden. \n\n Die wahrscheinliste Ursache für den Fehler ist, dass für eine beliebige Mannschaft im Verein noch keine Aufstellung verfügbar ist. Versuche es erneut, wenn die Aufstellungen freigegeben und online sind.\n \n Das Programm wird beendet")
        exit()

    # Versuche, mögliche Ersatzstammspieler auslesen zu können
    try:
        # Auslesen der möglichen Ersatzspieler. Verteilt auf 2 Threads mit 2 Queues (für höhere Geschwindigkeit)
        t3 = Thread(target=tt_funktionen_gui.suche_moegliche_ersatzspieler, args=(link_vereinsuebersicht1, team1_pick_link, verbandslink_gui, spielername1, out_queue3))
        t4 = Thread(target=tt_funktionen_gui.suche_moegliche_ersatzspieler, args=(link_vereinsuebersicht2, team2_pick_link, verbandslink_gui, spielername2, out_queue4))
        t3.start()
        t4.start()
        progress(progressbar, t3, t4, queue)
        t3.join()
        t4.join()

        ersatzspielernummer1 = out_queue3.get()
        ersatzspielername1 = out_queue3.get()
        ersatzspielerlpz1 = out_queue3.get()
        ersatzspielerbemerkung1 = out_queue3.get()
        ersatzspielerinaktivitaet1 = out_queue3.get()
        fehlendeaufstellung1 = out_queue3.get()

        ersatzspielernummer2 = out_queue4.get()
        ersatzspielername2 = out_queue4.get()
        ersatzspielerlpz2 = out_queue4.get()
        ersatzspielerbemerkung2 = out_queue4.get()
        ersatzspielerinaktivitaet2 = out_queue4.get()
        fehlendeaufstellung2 = out_queue4.get()

        # Offset zu Spielernummern der Stammaufstellung
        ersatzspielernummer1 = [x + max(spielernummer1) for x in ersatzspielernummer1]
        ersatzspielernummer2 = [x + max(spielernummer2) for x in ersatzspielernummer2]

        if fehlendeaufstellung1:
            messagebox.showinfo(title="Achtung", message="Es konnten nicht alle Aufstellungen der Mannschaften des Heimvereins ausgelsesen werden.\n\nDies ist notwendig, um mögliche Ersatzspieler/-innen für die Mannschaft herausfinden zu können. \n\n Die wahrscheinliste Ursache für den Fehler ist, dass für eine die betroffenen Mannschaft noch keine Aufstellung verfügbar ist. \n \nFür folgende Aufstellungen konnten nicht ermittelt werden:\n\n" + ', '.join(fehlendeaufstellung1))
        if fehlendeaufstellung2:
            messagebox.showinfo(title="Achtung", message="Es konnten nicht alle Aufstellungen der Mannschaften des Gastvereins ausgelsesen werden.\n\nDies ist notwendig, um mögliche Ersatzspieler/-innen für die Mannschaft herausfinden zu können. \n\n Die wahrscheinliste Ursache für den Fehler ist, dass für eine die betroffenen Mannschaft noch keine Aufstellung verfügbar ist. \n \nFür folgende Aufstellungen konnten nicht ermittelt werden:\n\n" + ', '.join(fehlendeaufstellung2))

    except:
        ersatzspielernummer1, ersatzspielername1, ersatzspielerlpz1, ersatzspielerbemerkung1, ersatzspielerinaktivitaet1 = [] , [], [], [], []
        ersatzspielernummer2, ersatzspielername2, ersatzspielerlpz2, ersatzspielerbemerkung2, ersatzspielerinaktivitaet2 = [] , [], [], [], []
        tk.messagebox.showinfo(title="Achtung", message="Es konnten nicht alle Aufstellungen aller Mannschaften des Vereins ausgelsesen werden. Dies ist notwendig, um mögliche Ersatzspieler/-innen für die Mannschaft herausfinden zu können. \n\n Die wahrscheinliste Ursache für den Fehler ist, dass für eine beliebige Mannschaft im Verein noch keine Aufstellung verfügbar ist. \n \n Der Fehler wird umgangen, indem nur die Stammspieler in der Aufstellung berücksichtigt werden können.")

    progressbar.stop()
    progressbar.place_forget()

    moegliche_spielername1 = spielername1 + ersatzspielername1
    moegliche_spielerlpz1 = spielerlpz1 + ersatzspielerlpz1
    moegliche_spielerbemerkung1 = spielerbemerkung1 + ersatzspielerbemerkung1
    moegliche_spielernummer1 = spielernummer1 + ersatzspielernummer1
    moegliche_spielerinaktivitaet1 = spielerinaktivitaet1 + ersatzspielerinaktivitaet1

    moegliche_spielername2 = spielername2 + ersatzspielername2
    moegliche_spielerlpz2 = spielerlpz2 + ersatzspielerlpz2
    moegliche_spielerbemerkung2 = spielerbemerkung2 + ersatzspielerbemerkung2
    moegliche_spielernummer2 = spielernummer2 + ersatzspielernummer2
    moegliche_spielerinaktivitaet2 = spielerinaktivitaet2 + ersatzspielerinaktivitaet2


    update_dropdown_spieler_heim(moegliche_spielername1)
    update_dropdown_spieler_ausw(moegliche_spielername2)

    # Label in Spieleranzeige schreiben
    ttk.Label(frame_topmid, text="U16", state='disabled').place(relx=0.605, rely=0.22)
    ttk.Label(frame_topmid, text="U21", state='disabled').place(relx=0.68, rely=0.22)
    ttk.Label(frame_topmid, text="LPZ", state='disabled').place(relx=0.80, rely=0.22)
    ttk.Label(frame_topright, text="U16", state='disabled').place(relx=0.605, rely=0.22)
    ttk.Label(frame_topright, text="U21", state='disabled').place(relx=0.68, rely=0.22)
    ttk.Label(frame_topright, text="LPZ", state='disabled').place(relx=0.80, rely=0.22)

    # Checkboxen für Alter definieren
    Checkage_U16_11 = tk.IntVar()
    Checkage_U16_12 = tk.IntVar()
    Checkage_U16_13 = tk.IntVar()
    Checkage_U16_14 = tk.IntVar()
    Checkage_U16_15 = tk.IntVar()
    Checkage_U16_16 = tk.IntVar()
    Checkage_U16_21 = tk.IntVar()
    Checkage_U16_22 = tk.IntVar()
    Checkage_U16_23 = tk.IntVar()
    Checkage_U16_24 = tk.IntVar()
    Checkage_U16_25 = tk.IntVar()
    Checkage_U16_26 = tk.IntVar()

    Checkage_U21_11 = tk.IntVar()
    Checkage_U21_12 = tk.IntVar()
    Checkage_U21_13 = tk.IntVar()
    Checkage_U21_14 = tk.IntVar()
    Checkage_U21_15 = tk.IntVar()
    Checkage_U21_16 = tk.IntVar()
    Checkage_U21_21 = tk.IntVar()
    Checkage_U21_22 = tk.IntVar()
    Checkage_U21_23 = tk.IntVar()
    Checkage_U21_24 = tk.IntVar()
    Checkage_U21_25 = tk.IntVar()
    Checkage_U21_26 = tk.IntVar()

    ttk.Checkbutton(frame_topmid, variable = Checkage_U16_11, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(1,1)).place(relx=0.625, rely=0.315)
    ttk.Checkbutton(frame_topmid, variable = Checkage_U16_12, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(2,1)).place(relx=0.625, rely=0.425)
    ttk.Checkbutton(frame_topmid, variable = Checkage_U16_13, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(3,1)).place(relx=0.625, rely=0.535)

    ttk.Checkbutton(frame_topmid, variable = Checkage_U21_11, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(1,1)).place(relx=0.695, rely=0.315)
    ttk.Checkbutton(frame_topmid, variable = Checkage_U21_12, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(2,1)).place(relx=0.695, rely=0.425)
    ttk.Checkbutton(frame_topmid, variable = Checkage_U21_13, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(3,1)).place(relx=0.695, rely=0.535)

    ttk.Checkbutton(frame_topright, variable = Checkage_U16_21, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(1,2)).place(relx=0.625, rely=0.315)
    ttk.Checkbutton(frame_topright, variable = Checkage_U16_22, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(2,2)).place(relx=0.625, rely=0.425)
    ttk.Checkbutton(frame_topright, variable = Checkage_U16_23, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(3,2)).place(relx=0.625, rely=0.535)

    ttk.Checkbutton(frame_topright, variable = Checkage_U21_21, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(1,2)).place(relx=0.695, rely=0.315)
    ttk.Checkbutton(frame_topright, variable = Checkage_U21_22, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(2,2)).place(relx=0.695, rely=0.425)
    ttk.Checkbutton(frame_topright, variable = Checkage_U21_23, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(3,2)).place(relx=0.695, rely=0.535)

    if spielsystem == 4 or spielsystem == 6:
        ttk.Checkbutton(frame_topmid, variable = Checkage_U16_14, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(4,1)).place(relx=0.625, rely=0.645)
        ttk.Checkbutton(frame_topmid, variable = Checkage_U21_14, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(4,1)).place(relx=0.695, rely=0.645)
        ttk.Checkbutton(frame_topright, variable = Checkage_U16_24, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(4,2)).place(relx=0.625, rely=0.645)
        ttk.Checkbutton(frame_topright, variable = Checkage_U21_24, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(4,2)).place(relx=0.695, rely=0.645)

    if spielsystem == 6:
        ttk.Checkbutton(frame_topmid, variable = Checkage_U16_15, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(5,1)).place(relx=0.625, rely=0.755)
        ttk.Checkbutton(frame_topmid, variable = Checkage_U16_16, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(6,1)).place(relx=0.625, rely=0.865)
        ttk.Checkbutton(frame_topmid, variable = Checkage_U21_15, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(5,1)).place(relx=0.695, rely=0.755)
        ttk.Checkbutton(frame_topmid, variable = Checkage_U21_16, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(6,1)).place(relx=0.695, rely=0.865)
        ttk.Checkbutton(frame_topright, variable = Checkage_U16_25, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(5,2)).place(relx=0.625, rely=0.755)
        ttk.Checkbutton(frame_topright, variable = Checkage_U16_26, onvalue = 1, offvalue = 0, command = lambda:resetplayerage21(6,2)).place(relx=0.625, rely=0.865)
        ttk.Checkbutton(frame_topright, variable = Checkage_U21_25, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(5,2)).place(relx=0.695, rely=0.755)
        ttk.Checkbutton(frame_topright, variable = Checkage_U21_26, onvalue = 1, offvalue = 0, command = lambda:resetplayerage16(6,2)).place(relx=0.695, rely=0.865)

    ttk.Label(frame_topmid, textvariable=LABEL_LPZ11, state='disabled').place(relx=0.785, rely=0.32)
    ttk.Label(frame_topmid, textvariable=LABEL_LPZ12, state='disabled').place(relx=0.785, rely=0.43)
    ttk.Label(frame_topmid, textvariable=LABEL_LPZ13, state='disabled').place(relx=0.785, rely=0.54)
    ttk.Label(frame_topmid, textvariable=LABEL_LPZ14, state='disabled').place(relx=0.785, rely=0.65)
    ttk.Label(frame_topmid, textvariable=LABEL_LPZ15, state='disabled').place(relx=0.785, rely=0.76)
    ttk.Label(frame_topmid, textvariable=LABEL_LPZ16, state='disabled').place(relx=0.785, rely=0.87)

    ttk.Label(frame_topright, textvariable=LABEL_LPZ21, state='disabled').place(relx=0.785, rely=0.32)
    ttk.Label(frame_topright, textvariable=LABEL_LPZ22, state='disabled').place(relx=0.785, rely=0.43)
    ttk.Label(frame_topright, textvariable=LABEL_LPZ23, state='disabled').place(relx=0.785, rely=0.54)
    ttk.Label(frame_topright, textvariable=LABEL_LPZ24, state='disabled').place(relx=0.785, rely=0.65)
    ttk.Label(frame_topright, textvariable=LABEL_LPZ25, state='disabled').place(relx=0.785, rely=0.76)
    ttk.Label(frame_topright, textvariable=LABEL_LPZ26, state='disabled').place(relx=0.785, rely=0.87)

    # Verhalten der Auswahlmenus der Spieler definieren
    team1spieler1.trace("w", write_lpz)
    team1spieler2.trace("w", write_lpz)
    team1spieler3.trace("w", write_lpz)
    team1spieler4.trace("w", write_lpz)
    team1spieler5.trace("w", write_lpz)
    team1spieler6.trace("w", write_lpz)
    team2spieler1.trace("w", write_lpz)
    team2spieler2.trace("w", write_lpz)
    team2spieler3.trace("w", write_lpz)
    team2spieler4.trace("w", write_lpz)
    team2spieler5.trace("w", write_lpz)
    team2spieler6.trace("w", write_lpz)

    # mögliche Spielernamen auslesen
    team1spieler1.set(moegliche_spielername1[0])
    team1spieler2.set(moegliche_spielername1[1])
    team1spieler3.set(moegliche_spielername1[2])
    if spielsystem == 4 or spielsystem == 6:
        team1spieler4.set(moegliche_spielername1[3])
    if spielsystem == 6:
        team1spieler5.set(moegliche_spielername1[4])
        team1spieler6.set(moegliche_spielername1[5])

    team2spieler1.set(moegliche_spielername2[0])
    team2spieler2.set(moegliche_spielername2[1])
    team2spieler3.set(moegliche_spielername2[2])
    if spielsystem == 4 or spielsystem == 6:
        team2spieler4.set(moegliche_spielername2[3])
    if spielsystem == 6:
        team2spieler5.set(moegliche_spielername2[4])
        team2spieler6.set(moegliche_spielername2[5])

    Anweisung.set("Bitte Aufstellung auswählen und anschließend auf \"Spielprognose berechnen\" klicken.")

    menubutton1.config(state=tk.NORMAL)

    dropdown_team1spieler1.config(state=tk.NORMAL)
    dropdown_team1spieler2.config(state=tk.NORMAL)
    dropdown_team1spieler3.config(state=tk.NORMAL)
    dropdown_team2spieler1.config(state=tk.NORMAL)
    dropdown_team2spieler2.config(state=tk.NORMAL)
    dropdown_team2spieler3.config(state=tk.NORMAL)

    if spielsystem == 4 or spielsystem == 6:
        dropdown_team1spieler4.config(state=tk.NORMAL)
        dropdown_team2spieler4.config(state=tk.NORMAL)

    if spielsystem == 6:
        dropdown_team1spieler5.config(state=tk.NORMAL)
        dropdown_team2spieler5.config(state=tk.NORMAL)
        dropdown_team1spieler6.config(state=tk.NORMAL)
        dropdown_team2spieler6.config(state=tk.NORMAL)


def resetplayerage21(spielernummer, team):
    # Wenn Checkboxen bezüglich Alter angeklickt werden, soll die jeweils andere deselektiert werden. Dies wird mit dieser Funktion erreicht
    if spielernummer==1 and team == 1 and Checkage_U16_11.get() == 1:
        Checkage_U21_11.set(0)
    elif spielernummer==2 and team == 1 and Checkage_U16_12.get() == 1:
        Checkage_U21_12.set(0)
    elif spielernummer==3 and team == 1 and Checkage_U16_13.get() == 1:
        Checkage_U21_13.set(0)
    elif spielernummer==4 and team == 1 and Checkage_U16_14.get() == 1:
        Checkage_U21_14.set(0)
    elif spielernummer==5 and team == 1 and Checkage_U16_15.get() == 1:
        Checkage_U21_15.set(0)
    elif spielernummer==6 and team == 1 and Checkage_U16_16.get() == 1:
        Checkage_U21_16.set(0)
    elif spielernummer==1 and team == 2 and Checkage_U16_21.get() == 1:
        Checkage_U21_21.set(0)
    elif spielernummer==2 and team == 2 and Checkage_U16_22.get() == 1:
        Checkage_U21_22.set(0)
    elif spielernummer==3 and team == 2 and Checkage_U16_23.get() == 1:
        Checkage_U21_23.set(0)
    elif spielernummer==4 and team == 2 and Checkage_U16_24.get() == 1:
        Checkage_U21_24.set(0)
    elif spielernummer==5 and team == 2 and Checkage_U16_25.get() == 1:
        Checkage_U21_25.set(0)
    elif spielernummer==6 and team == 2 and Checkage_U16_26.get() == 1:
        Checkage_U21_26.set(0)


def resetplayerage16(spielernummer, team):
    # Wenn Checkboxen bezüglich Alter angeklickt werden, soll die jeweils andere deselektiert werden. Dies wird mit dieser Funktion erreicht
    if spielernummer==1 and team == 1 and Checkage_U21_11.get() == 1:
        Checkage_U16_11.set(0)
    elif spielernummer==2 and team == 1 and Checkage_U21_12.get() == 1:
        Checkage_U16_12.set(0)
    elif spielernummer==3 and team == 1 and Checkage_U21_13.get() == 1:
        Checkage_U16_13.set(0)
    elif spielernummer==4 and team == 1 and Checkage_U21_14.get() == 1:
        Checkage_U16_14.set(0)
    elif spielernummer==5 and team == 1 and Checkage_U21_15.get() == 1:
        Checkage_U16_15.set(0)
    elif spielernummer==6 and team == 1 and Checkage_U21_16.get() == 1:
        Checkage_U16_16.set(0)
    elif spielernummer==1 and team == 2 and Checkage_U21_21.get() == 1:
        Checkage_U16_21.set(0)
    elif spielernummer==2 and team == 2 and Checkage_U21_22.get() == 1:
        Checkage_U16_22.set(0)
    elif spielernummer==3 and team == 2 and Checkage_U21_23.get() == 1:
        Checkage_U16_23.set(0)
    elif spielernummer==4 and team == 2 and Checkage_U21_24.get() == 1:
        Checkage_U16_24.set(0)
    elif spielernummer==5 and team == 2 and Checkage_U21_25.get() == 1:
        Checkage_U16_25.set(0)
    elif spielernummer==6 and team == 2 and Checkage_U21_26.get() == 1:
        Checkage_U16_26.set(0)


def getplayerage():
    playerage1 = []
    playerage2 = []
    
    # Prüfen, ob Altersangaben gesetzt wurden
    # Team1 
    if Checkage_U16_11.get() == 1:
        playerage1.append(4)
    elif Checkage_U21_11.get() == 1:
        playerage1.append(2)
    else:
        playerage1.append(0)
        
    if Checkage_U16_12.get() == 1:
        playerage1.append(4)
    elif Checkage_U21_12.get() == 1:
        playerage1.append(2)
    else:
        playerage1.append(0)
                
    if Checkage_U16_13.get() == 1:
        playerage1.append(4)
    elif Checkage_U21_13.get() == 1:
        playerage1.append(2)
    else:
        playerage1.append(0)
    
    if spielsystem == 4 or spielsystem == 6:
        if Checkage_U16_14.get() == 1:
            playerage1.append(4)
        elif Checkage_U21_14.get() == 1:
            playerage1.append(2)
        else:
            playerage1.append(0)
    
    if spielsystem == 6:
        if Checkage_U16_15.get() == 1:
            playerage1.append(4)
        elif Checkage_U21_15.get() == 1:
            playerage1.append(2)
        else:
            playerage1.append(0)
                    
        if Checkage_U16_16.get() == 1:
            playerage1.append(4)
        elif Checkage_U21_16.get() == 1:
            playerage1.append(2)
        else:
            playerage1.append(0)
    
    # Team2
    if Checkage_U16_21.get() == 1:
        playerage2.append(4)
    elif Checkage_U21_21.get() == 1:
        playerage2.append(2)
    else:
        playerage2.append(0)
        
    if Checkage_U16_22.get() == 1:
        playerage2.append(4)
    elif Checkage_U21_22.get() == 1:
        playerage2.append(2)
    else:
        playerage2.append(0)
                
    if Checkage_U16_23.get() == 1:
        playerage2.append(4)
    elif Checkage_U21_23.get() == 1:
        playerage2.append(2)
    else:
        playerage2.append(0)
    
    if spielsystem == 4 or spielsystem == 6:
        if Checkage_U16_24.get() == 1:
            playerage2.append(4)
        elif Checkage_U21_24.get() == 1:
            playerage2.append(2)
        else:
            playerage2.append(0)
    
    if spielsystem == 6:
        if Checkage_U16_25.get() == 1:
            playerage2.append(4)
        elif Checkage_U21_25.get() == 1:
            playerage2.append(2)
        else:
            playerage2.append(0)
                    
        if Checkage_U16_26.get() == 1:
            playerage2.append(4)
        elif Checkage_U21_26.get() == 1:
            playerage2.append(2)
        else:
            playerage2.append(0)
            
    return playerage1, playerage2


def gamecalc(*args):
    global spielstand_heim, spielstand_ausw, placestep, lpz1_1gewinnt, lpz1_1verliert, lpz2_2gewinnt, lpz2_2verliert, wahrscheinlichkeit, gewonnene_spiele, Summe_lpz_team_1, Summe_lpz_team_2, ansetungsmatrix, spielerlpz1, spielerlpz2, spielerteam1_lpz_gesamt, spielerteam2_lpz_gesamt
    
    # Labeltexte
    global Doppelergebnis, Label_Endstand
    
    ttk.Label(frame_bottom, text="                                                                                                  " , state='normal', foreground = "grey", font=('Helvetica', 16, 'bold')).place(relx=0.5, rely=0.5, anchor = tk.CENTER)
    
    # Prüfen, ob Altersangaben gesetzt wurden
    playerage1, playerage2 = getplayerage()
    
     # Überprüfunge der Eingabe der Spieler (Existenz, Reihenfolge, Doppelungen)
    playernumberteam1 = []
    playernumberteam2 = []
    
    # Spielernamen aus GUI auslesen
    playernumberteam1.append(getplayernumber(team1spieler1.get(),1))
    playernumberteam1.append(getplayernumber(team1spieler2.get(),1))
    playernumberteam1.append(getplayernumber(team1spieler3.get(),1))
    if spielsystem == 4 or spielsystem == 6:
        playernumberteam1.append(getplayernumber(team1spieler4.get(),1))
    if spielsystem == 6:
        playernumberteam1.append(getplayernumber(team1spieler5.get(),1))
        playernumberteam1.append(getplayernumber(team1spieler6.get(),1))
        
    playernumberteam2.append(getplayernumber(team2spieler1.get(),2))
    playernumberteam2.append(getplayernumber(team2spieler2.get(),2))
    playernumberteam2.append(getplayernumber(team2spieler3.get(),2))
    if spielsystem == 4 or spielsystem == 6:
        playernumberteam2.append(getplayernumber(team2spieler4.get(),2))
    if spielsystem == 6:
        playernumberteam2.append(getplayernumber(team2spieler5.get(),2))
        playernumberteam2.append(getplayernumber(team2spieler6.get(),2))
    
    if(playernumberteam1 != sorted(playernumberteam1)):
        tk.messagebox.showinfo(title="Achtung", message="Die Aufstellung der Heimmannschaft erfolgte in der falschen Reihenfolge. Bitte korrigieren.")
        return
        
    if(playernumberteam2 != sorted(playernumberteam2)):
        tk.messagebox.showinfo(title="Achtung", message="Die Aufstellung der Gastmannschaft erfolgte in der falschen Reihenfolge. Bitte korrigieren.")
        return
        
    # Prüfen, ob Spielereintraege fehlen. Falls ja, Eingabe wiederholen lassen
    if -1 in playernumberteam1 :
        tk.messagebox.showinfo(title="Achtung", message="Für die Heimmannschaft wurden noch nicht alle Spieler/-innen ausgewaehlt.")
        return
        
    if -1 in playernumberteam2 :
        tk.messagebox.showinfo(title="Achtung", message="Für die Gastmannschaft wurden noch nicht alle Spieler/-innen ausgewaehlt.")
        return

    # Prüfen, ob doppelte Eintraege vorhanden sind.
    if len(playernumberteam1) != len(set(playernumberteam1)):
        tk.messagebox.showinfo(title="Achtung", message="Für die Heimmannschaft wurden Spieler/-innen mehrfach ausgewaehlt.")
        return
    if len(playernumberteam2) != len(set(playernumberteam2)):
        tk.messagebox.showinfo(title="Achtung", message="Für die Gastmannschaft wurden Spieler/-innen mehrfach ausgewaehlt.")
        return
    
    # Linien in "Tabelle" zeichnen
    canvas = tk.Canvas(frame_bottom,width=w_width, height=2*w_high/3, bg=color)
    #horizontal
    canvas.place(relx=0.0, rely=0.0)
    canvas.create_line(50,60,w_width-55,60,fill="black")
    canvas.create_line(50,90,w_width-55,90,fill="black")
    #vertikal
    canvas.create_line(425,60,425,380,fill="black")
    canvas.create_line(505,60,505,380,fill="black")
    canvas.create_line(930,60,930,380,fill="black")
    canvas.create_line(50,60,50,380,fill="black")
    canvas.create_line(w_width-55,60,w_width-55,380,fill="black")

    
    # Header der "Tabelle" setzen
    Header0 = ttk.Label(frame_bottom, text="Basierdend auf den aktuellen LPZ-Werten wurde folgender Spielausgang prognostiziert:" , state='normal', foreground = "grey", font=('Helvetica', 11, 'bold')).place(x = 50, y=2000/3*0.04)
    Header1 = ttk.Label(frame_bottom, text=team1auswahl.get() , state='normal', font=('Helvetica', 12, 'bold')).place(relx=xlineheim, y=2000/3*0.10)
    Header2 = ttk.Label(frame_bottom, text="LPZ" , state='normal', font=('Helvetica', 11, 'bold'), foreground = "grey").place(relx=xlinelpz1, y=2000/3*0.10)
    Header3 = ttk.Label(frame_bottom, text=team2auswahl.get() , state='normal', font=('Helvetica', 12, 'bold')).place(relx=xlineausw, y=2000/3*0.10)
    Header4 = ttk.Label(frame_bottom, text="LPZ" , state='normal', font=('Helvetica', 11, 'bold'), foreground = "grey").place(relx=xlinelpz2, y=2000/3*0.10)
    Header5 = ttk.Label(frame_bottom, text="Ergebnis" , state='normal', font=('Helvetica', 11, 'bold')).place(relx=xlineergebnis-0.01, y=2000/3*0.10)
    Header6 = ttk.Label(frame_bottom, text="Wkt" , state='normal', font=('Helvetica', 11, 'bold')).place(relx=xlinewkt+0.003, y=2000/3*0.10)
    Header7 = ttk.Label(frame_bottom, text="Spielstand" , state='normal', font=('Helvetica', 11, 'bold')).place(relx=xlinestand-0.014, y=2000/3*0.10)
    Header8 = ttk.Label(frame_bottom, text="Gesamt:" , state='normal', font=('Helvetica', 12, 'bold')).place(relx=xlineheim, rely=0.81)
    Header9 = ttk.Label(frame_bottom, text="Um einzelne Spielergebnisse zu drehen, bitte die entsprechenden Knöpfe neben den Einzelresultaten drücken." , state='normal', foreground = "grey", font=('Helvetica', 11, 'bold')).place(x = 50, y=w_high_bottom - 40)
    
    # andere Menüpunkte ausblenden
    dropdown_verband.config(state=tk.DISABLED)
    dropdown_liga.config(state=tk.DISABLED)
    dropdown_team1.config(state=tk.DISABLED)
    dropdown_team2.config(state=tk.DISABLED)
    
    dropdown_team1spieler1.config(state=tk.DISABLED)
    dropdown_team1spieler2.config(state=tk.DISABLED)
    dropdown_team1spieler3.config(state=tk.DISABLED)
    dropdown_team2spieler1.config(state=tk.DISABLED)
    dropdown_team2spieler2.config(state=tk.DISABLED)
    dropdown_team2spieler3.config(state=tk.DISABLED)
    dropdown_team1spieler4.config(state=tk.DISABLED)
    dropdown_team2spieler4.config(state=tk.DISABLED)
    dropdown_team1spieler5.config(state=tk.DISABLED)
    dropdown_team2spieler5.config(state=tk.DISABLED)
    dropdown_team1spieler6.config(state=tk.DISABLED)
    dropdown_team2spieler6.config(state=tk.DISABLED)
    
    # Start der eigentlichen Berechnung
    
    # Auslesen von Spieler, deren LPZ sowie Bemerkungen und eventuelle Inaktivitaet
    spielername1 = []
    spielerlpz1 = []
    spielerbemerkung1 = []
    spielerinaktiv1 = []
    spielername2 = []
    spielerlpz2 = []
    spielerbemerkung2 = []
    spielerinaktiv2 = []
    spielstand_heim = []
    spielstand_ausw = []
    
    for spieler1 in playernumberteam1:
        spielername1.append(moegliche_spielername1[spieler1-1])
        spielerlpz1.append(moegliche_spielerlpz1[spieler1-1])
        spielerbemerkung1.append(moegliche_spielerbemerkung1[spieler1-1])
        spielerinaktiv1.append(moegliche_spielerinaktivitaet1[spieler1-1])
    for spieler2 in playernumberteam2:
        spielername2.append(moegliche_spielername2[spieler2-1])
        spielerlpz2.append(moegliche_spielerlpz2[spieler2-1])
        spielerbemerkung2.append(moegliche_spielerbemerkung2[spieler2-1])
        spielerinaktiv2.append(moegliche_spielerinaktivitaet2[spieler1-1])

    ansetungsmatrix = tt_funktionen_gui.ansetzungen(spielsystem)

    gewonnene_spiele = [0, 0]

    Summe_lpz_team_1 = 0
    Summe_lpz_team_2 = 0

    for i in spielerlpz1[0:spielsystem]:
        Summe_lpz_team_1 += int(i)

    for i in spielerlpz2[0:spielsystem]:
        Summe_lpz_team_2 += int(i)
        

    # Doppelergebnis festlegen. Mannschaft mit einem höheren LPZ gewinnt mehr Doppel und bekommt ein gewonnenenes Spiel zugeschrieben
    if Summe_lpz_team_1 > Summe_lpz_team_2 and abs(Summe_lpz_team_1-Summe_lpz_team_2) >= 100:
        if spielsystem == 4:
            gewonnene_spiele[0] += 2
            spielstand_heim.append(2)
            spielstand_ausw.append(0)
        elif spielsystem == 6:
            gewonnene_spiele[0] += 3
            spielstand_heim.append(3)
            spielstand_ausw.append(0)
        elif spielsystem == 3:
            gewonnene_spiele[0] += 1
            spielstand_heim.append(1)
            spielstand_ausw.append(0)
    elif abs(Summe_lpz_team_1 - Summe_lpz_team_2) < 100:
        if spielsystem == 4:
            gewonnene_spiele[0] += 1
            gewonnene_spiele[1] += 1
            spielstand_heim.append(1)
            spielstand_ausw.append(1)
        elif spielsystem == 6:
            gewonnene_spiele[0] += 2
            gewonnene_spiele[1] += 1
            spielstand_heim.append(2)
            spielstand_ausw.append(1)
        elif spielsystem == 3:
            gewonnene_spiele[0] += 1
            spielstand_heim.append(1)
            spielstand_ausw.append(0)
    elif Summe_lpz_team_1 < Summe_lpz_team_2 and abs(Summe_lpz_team_1-Summe_lpz_team_2) >= 100:
        if spielsystem == 4:
            gewonnene_spiele[1] += 2
            spielstand_heim.append(0)
            spielstand_ausw.append(2)
        elif spielsystem == 6:
            gewonnene_spiele[1] += 3
            spielstand_heim.append(0)
            spielstand_ausw.append(3)
        elif spielsystem == 3:
            gewonnene_spiele[1] += 1
            spielstand_heim.append(0)
            spielstand_ausw.append(1)
    
    # Doppelergebnis in Fenster schreiben
    Doppelergebnis = tk.StringVar()
    Doppelergebnis.set(str(spielstand_heim[0]) + " : " + str(spielstand_ausw[0]))
    
    ttk.Label(frame_bottom, text="Vermutlicher Spielstand nach Doppel(n) ", state='normal').place(relx=xlineergebnis, y=2000/3*0.04)
    ttk.Label(frame_bottom, textvariable=Doppelergebnis , state='normal').place(relx=xlinestand, y=2000/3*0.04)
    ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(1, ansetungsmatrix, 0)).place(relx=xlinestand+0.035, y=2000/3*0.0354)

    # Label für Endstand
    Label_Endstand = tk.StringVar()
    ttk.Label(frame_bottom, textvariable=Label_Endstand, state='normal', font=('Helvetica', 12, 'bold')).place(relx=xlinestand-0.0025, y=w_high_bottom - 40)
    
    ttk.Label(frame_bottom, text="Endstand", state='normal', font=('Helvetica', 12, 'bold')).place(relx=xlinestand - 0.08, y=w_high_bottom - 40)

    # Einzelspiele berechnen
    placement = -1
    placestep = 0.035
    
    wahrscheinlichkeit = []
    lpz1_1gewinnt = []
    lpz1_1verliert = []
    lpz2_2gewinnt = []
    lpz2_2verliert = []
    spielerteam1_lpz_gesamt = []
    spielerteam2_lpz_gesamt = []
    
    
    for spielernummera, spielernummerb in ansetungsmatrix:
        placement += 1
        i = placement
        
        lpzdiff1_1gewinnt, lpzdiff1_1verliert, lpzdiff2_2gewinnt, lpzdiff2_2verliert = tt_funktionen_gui.berechne_lpzdifferenz(spielerlpz1[spielernummera - 1], spielerlpz2[spielernummerb - 1], playerage1[spielernummera - 1], playerage2[spielernummerb - 1], spielerinaktiv1[spielernummera - 1], spielerinaktiv2[spielernummerb - 1])
        wkt = tt_funktionen_gui.berechne_siegwahrscheinlichkeit(spielerlpz1[spielernummera - 1], spielerlpz2[spielernummerb - 1])
        
        wahrscheinlichkeit.append(wkt)
        lpz1_1gewinnt.append(lpzdiff1_1gewinnt)
        lpz1_1verliert.append(lpzdiff1_1verliert)
        lpz2_2gewinnt.append(lpzdiff2_2gewinnt)
        lpz2_2verliert.append(lpzdiff2_2verliert)

        # Zwischenlinien einfügen
        if placement > 0:
            if platform == "linux" or platform == "linux2":
                canvas.create_line(50,2000/3*(0.145+placement*placestep),w_width-55,2000/3*(0.145+placement*placestep),fill="grey70")
            elif platform == "win32":
                canvas.create_line(50,2000/3*(0.145+placement*placestep),w_width-55,2000/3*(0.145+placement*placestep),fill="grey80")
            
        
        # Buttons platzieren
        if placement == 0:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(2, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(2, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
        elif placement == 1:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(3, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(3, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
        elif placement == 2:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(4, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(4, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
        elif placement == 3:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(5, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(5, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
        elif placement == 4:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(6, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(6, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))       
        elif placement == 5:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(7, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(7, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
        elif placement == 6:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(8, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(8, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
        elif placement == 7:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(9, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(9, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
        elif placement == 8:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(10, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(10, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
        elif placement == 9:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(11, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(11, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
        elif placement == 10:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(12, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(12, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
        elif placement == 11:
            if spielerlpz1[spielernummera - 1] >= spielerlpz2[spielernummerb - 1]:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(13, ansetungsmatrix,1)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))
            else:
                ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(13, ansetungsmatrix,2)).place(relx=xlinebutton, y=2000/3*(0.148+placement*placestep))

        # Ergebnisse ausgeben
        if int(spielerlpz1[spielernummera-1]) >= int(spielerlpz2[spielernummerb-1]):
            gewonnene_spiele[0] += 1
            spielstand_heim.append(1)
            spielstand_ausw.append(0)
            
            ttk.Label(frame_bottom, text=(spielername1[spielernummera - 1]) , state='normal').place(relx=xlineheim, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(spielerlpz1[spielernummera - 1]) , state='normal', foreground = "grey").place(relx=xlinelpz1, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text="+" + str(lpzdiff1_1gewinnt) , state='normal', foreground = "green").place(relx=xlinelpzdiff1, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text="E" + str(spielernummera) + " | E" + str(spielernummerb) + "  ", state='normal', foreground = "gray").place(relx=xlinegame, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=spielername2[spielernummerb - 1] , state='normal').place(relx=xlineausw, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(spielerlpz2[spielernummerb - 1]) , state='normal', foreground = "grey").place(relx=xlinelpz2, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(lpzdiff2_2verliert) , state='normal', foreground = "red").place(relx=xlinelpzdiff2, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text="1 : 0", state='normal', foreground = "green", font=('Helvetica', 11, 'bold')).place(relx=xlineergebnis, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(int(tt_funktionen_gui.berechne_siegwahrscheinlichkeit(spielerlpz1[spielernummera - 1], spielerlpz2[spielernummerb - 1]))) + " %  " , state='normal').place(relx=xlinewkt, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(sum(spielstand_heim[:placement+2])) + " : " + str(sum(spielstand_ausw[:placement+2])) + "  " , state='normal',font=('Helvetica', 11, 'bold')).place(relx=xlinestand, y=2000/3*(0.15+placement*placestep))
            
            spielerteam1_lpz_gesamt.append(lpzdiff1_1gewinnt)
            spielerteam2_lpz_gesamt.append(lpzdiff2_2verliert)
            
        elif int(spielerlpz1[spielernummera-1]) < int(spielerlpz2[spielernummerb-1]):
            gewonnene_spiele[1] += 1
            spielstand_heim.append(0)
            spielstand_ausw.append(1)

            ttk.Label(frame_bottom, text=(spielername1[spielernummera - 1]) , state='normal').place(relx=xlineheim, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(spielerlpz1[spielernummera - 1]) , state='normal', foreground = "grey").place(relx=xlinelpz1, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(lpzdiff1_1verliert) , state='normal', foreground = "red").place(relx=xlinelpzdiff1, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text="E" + str(spielernummera) + " | E" + str(spielernummerb) + "  ", state='normal', foreground = "gray").place(relx=xlinegame, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=spielername2[spielernummerb - 1] , state='normal').place(relx=xlineausw, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(spielerlpz2[spielernummerb - 1]) , state='normal', foreground = "grey").place(relx=xlinelpz2, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text="+" + str(lpzdiff2_2gewinnt) , state='normal', foreground = "green").place(relx=xlinelpzdiff2, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text="0 : 1" , state='normal', foreground = "red", font=('Helvetica', 11, 'bold')).place(relx=xlineergebnis, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(int(tt_funktionen_gui.berechne_siegwahrscheinlichkeit(spielerlpz1[spielernummera - 1], spielerlpz2[spielernummerb - 1]))) + " %  " , state='normal').place(relx=xlinewkt, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(sum(spielstand_heim[:placement+2])) + " : " + str(sum(spielstand_ausw[:placement+2])) + "  " , state='normal',font=('Helvetica', 11, 'bold')).place(relx=xlinestand, y=2000/3*(0.15+placement*placestep))
            
            spielerteam1_lpz_gesamt.append(lpzdiff1_1verliert)
            spielerteam2_lpz_gesamt.append(lpzdiff2_2gewinnt)

    ttk.Label(frame_bottom, text= "Ø" + str(int(sum(spielerlpz1)/len(spielerlpz1))) + "   ", foreground = "grey",font=('Helvetica', 12, 'bold')).place(relx=(xlinelpz1 - 0.008), rely=0.81)
    ttk.Label(frame_bottom, text= "Ø" + str(int(sum(spielerlpz2)/len(spielerlpz2))) + "   ", foreground = "grey",font=('Helvetica', 12, 'bold')).place(relx=(xlinelpz2 - 0.008), rely=0.81)
    
    # Ø

    # Prüfen, ob ein Entscheidungsdoppel gesetzt werden muss (im 6er-Spielsystem)
    entscheidungsdoppel()
    
    # Linie unter Ergebnissen setzen
    canvas.create_line(50,380,w_width-55,380,fill="black")
    canvas.create_line(w_width-320,425,w_width-55,425,fill="black")
    
    output_final_results()

def updateresults(game, ansetungsmatrix, winner):
    placement = game - 2
    
    # Erste Doppelspiele
    if game == 1:
        # Fall 3er-System
        if spielsystem == 3:
            if spielstand_heim[0] == 1:
                spielstand_heim[0] = 0
                spielstand_ausw[0] = 1
        
        elif spielsystem == 4:
            if spielstand_heim[0] == 2:
                spielstand_heim[0] = 1
                spielstand_ausw[0] = 1
            elif spielstand_heim[0] == 1:
                spielstand_heim[0] = 0
                spielstand_ausw[0] = 2
            elif spielstand_heim[0] == 0:
                spielstand_heim[0] = 2
                spielstand_ausw[0] = 0
        elif spielsystem == 6:
            if spielstand_heim[0] == 3:
                spielstand_heim[0] = 2
                spielstand_ausw[0] = 1
            elif spielstand_heim[0] == 2:
                spielstand_heim[0] = 1
                spielstand_ausw[0] = 2
            elif spielstand_heim[0] == 1:
                spielstand_heim[0] = 0
                spielstand_ausw[0] = 3
            elif spielstand_heim[0] == 0:
                spielstand_heim[0] = 3
                spielstand_ausw[0] = 0
        
        # Label für Doppel aktualisieren
        Doppelergebnis.set(str(spielstand_heim[0]) + " : " + str(spielstand_ausw[0]) + "  ")
    
    elif game == 14:
        #Entscheidungsdoppel im Fall einer 6er-Mannschaft
        if spielsystem == 6 and abs(sum(spielstand_heim[0:13]) - sum(spielstand_ausw[0:13])) == 1:
            if spielstand_heim[13] == 0 and spielstand_ausw[13] == 0:
                pass
            elif spielstand_heim[13] == 1 and spielstand_ausw[13] == 0:
                spielstand_heim[13] = 0
                spielstand_ausw[13] = 1
            elif spielstand_heim[13] == 0 and spielstand_ausw[13] == 1:
                spielstand_heim[13] = 1
                spielstand_ausw[13] = 0
            
            # Spielstand updaten
            ttk.Label(frame_bottom, text= str(spielstand_heim[13]) + " : " + str(spielstand_ausw[13]) + "   ", state='normal',font=('Helvetica', 11, 'bold')).place(relx=xlinestand, y=2000/3*0.595)
            output_final_results()
            return
        else:
            output_final_results()
            return
        
    else:
        #alle Einzel:
        if spielstand_heim[game-1] == 1:
            ttk.Label(frame_bottom, text=str(lpz1_1verliert[game-2]) + "    " , state='normal', foreground = "red").place(relx=xlinelpzdiff1, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text="+" + str(lpz2_2gewinnt[game-2]) + "    " , state='normal', foreground = "green").place(relx=xlinelpzdiff2, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text="0 : 1" , state='normal', foreground = "red", font=('Helvetica', 11, 'bold')).place(relx=xlineergebnis, y=2000/3*(0.15+placement*placestep))
            if winner == 1:
                ttk.Label(frame_bottom, text=str(int(100-wahrscheinlichkeit[game-2])) + "" , state='normal').place(relx=xlinewkt, y=2000/3*(0.15+placement*placestep))
            elif winner == 2:
                ttk.Label(frame_bottom, text=str(int(wahrscheinlichkeit[game-2])) + "" , state='normal').place(relx=xlinewkt, y=2000/3*(0.15+placement*placestep))
            spielstand_heim[game-1] = 0
            spielstand_ausw[game-1] = 1
            spielerteam1_lpz_gesamt[game-2] = lpz1_1verliert[game-2]
            spielerteam2_lpz_gesamt[game-2] = lpz2_2gewinnt[game-2]
            
        else:
            ttk.Label(frame_bottom, text="+" + str(lpz1_1gewinnt[game-2]) + "    ", state='normal', foreground = "green").place(relx=xlinelpzdiff1, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text=str(lpz2_2verliert[game-2]) + "    ", state='normal', foreground = "red").place(relx=xlinelpzdiff2, y=2000/3*(0.15+placement*placestep))
            ttk.Label(frame_bottom, text="1 : 0", state='normal', foreground = "green", font=('Helvetica', 11, 'bold')).place(relx=xlineergebnis, y=2000/3*(0.15+placement*placestep))
            if winner == 1:
                ttk.Label(frame_bottom, text=str(int(wahrscheinlichkeit[game-2])) + "" , state='normal').place(relx=xlinewkt, y=2000/3*(0.15+placement*placestep))
            elif winner == 2:
                ttk.Label(frame_bottom, text=str(int(100-wahrscheinlichkeit[game-2])) + "" , state='normal').place(relx=xlinewkt, y=2000/3*(0.15+placement*placestep))
            spielstand_heim[game-1] = 1
            spielstand_ausw[game-1] = 0
            spielerteam1_lpz_gesamt[game-2] = lpz1_1gewinnt[game-2]
            spielerteam2_lpz_gesamt[game-2] = lpz2_2verliert[game-2]
            
    
    # Prüfen, ob beim aktuellen Spielstand im 6er-System ein Entscheidungsdoppel gespielt werden muss.
    entscheidungsdoppel()
    output_final_results()
    
    # Label für Gesamtstand aktualisieren
    placement = -1
    
    # Gesamtspielstand nach jedem Spiel anzeigen
    for spielernummera, spielernummerb in ansetungsmatrix:
        placement += 1
        ttk.Label(frame_bottom, text=str(sum(spielstand_heim[:placement+2])) + " : " + str(sum(spielstand_ausw[:placement+2])) + "  ", state='normal',font=('Helvetica', 11, 'bold')).place(relx=xlinestand, y=2000/3*(0.15+placement*placestep))


def entscheidungsdoppel(*args):
    # Entscheidungsdoppel im 6er System abbilden mit Doppelsieg für insgesamt staerkere Mannschaft
    
    if spielsystem == 6 and abs(sum(spielstand_heim[0:13]) - sum(spielstand_ausw[0:13])) == 1:
        if Summe_lpz_team_1 >= Summe_lpz_team_2:
            gewonnene_spiele[0] += 1
            if len(spielstand_heim)==13:
                spielstand_heim.append(1)
                spielstand_ausw.append(0)
            else:
                spielstand_heim[13]=1
                spielstand_ausw[13]=0

        elif Summe_lpz_team_1 < Summe_lpz_team_2:
            gewonnene_spiele[1] += 1
            if len(spielstand_heim)==13:
                spielstand_heim.append(0)
                spielstand_ausw.append(1)
            else:
                spielstand_heim[13]=0
                spielstand_ausw[13]=1
        
        ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(14, ansetungsmatrix,0)).place(relx=xlinestand+0.035, y=2000/3*0.594)
        ttk.Label(frame_bottom, text="Entscheidungsdoppel", state='normal').place(relx=xlinestand - 0.11, y=2000/3*0.595)
        ttk.Label(frame_bottom, text= str(spielstand_heim[13]) + " : " + str(spielstand_ausw[13]) + "   " , state='normal', font=('Helvetica', 11, 'bold')).place(relx=xlinestand, y=2000/3*0.595)
    
    elif spielsystem == 6:
        ttk.Button(frame_bottom, image = switchimange, command=lambda:updateresults(14, ansetungsmatrix,0)).place(relx=xlinestand+0.035, y=2000/3*0.594)
        ttk.Label(frame_bottom, text="Entscheidungsdoppel", state='normal').place(relx=xlinestand - 0.11, y=2000/3*0.595)
        ttk.Label(frame_bottom, text= "          " , state='normal').place(relx=xlinestand, y=2000/3*0.595)
        if len(spielstand_heim)==13:
            spielstand_heim.append(0)
            spielstand_ausw.append(0)
        else:
            spielstand_heim[13]=0
            spielstand_ausw[13]=0


def output_final_results(*args):
    # Endstand
    Label_Endstand.set(str(sum(spielstand_heim)) + " : " + str(sum(spielstand_ausw)) + "   ")
    
    # GesamtLPZ ausgeben
    if sum(spielerteam1_lpz_gesamt)>0:
        ttk.Label(frame_bottom, text= "+" + str(sum(spielerteam1_lpz_gesamt)) + "   ", foreground = "green",font=('Helvetica', 12, 'bold')).place(relx=xlinelpzdiff1 - 0.005, rely=0.81)
    elif sum(spielerteam1_lpz_gesamt)<0:
        ttk.Label(frame_bottom, text= str(sum(spielerteam1_lpz_gesamt)) + "   ", foreground = "red",font=('Helvetica', 12, 'bold')).place(relx=xlinelpzdiff1 - 0.005, rely=0.81)
    else:
        ttk.Label(frame_bottom, text= "+-" + str(sum(spielerteam1_lpz_gesamt)) + "   ", font=('Helvetica', 12, 'bold')).place(relx=xlinelpzdiff1 - 0.005, rely=0.81)
    
    if sum(spielerteam2_lpz_gesamt)>0:
        ttk.Label(frame_bottom, text= "+" + str(sum(spielerteam2_lpz_gesamt)) + "   ", foreground = "green",font=('Helvetica', 12, 'bold')).place(relx=xlinelpzdiff2 - 0.005, rely=0.81)
    elif sum(spielerteam2_lpz_gesamt)<0:
        ttk.Label(frame_bottom, text= str(sum(spielerteam2_lpz_gesamt)) + "   ", foreground = "red",font=('Helvetica', 12, 'bold')).place(relx=xlinelpzdiff2 - 0.005, rely=0.81)
    else:
        ttk.Label(frame_bottom, text= "+-" + str(sum(spielerteam2_lpz_gesamt)) + "   ", font=('Helvetica', 12, 'bold')).place(relx=xlinelpzdiff2 - 0.005, rely=0.81)
        
    
    # LPZ-Differenzen der einzelnen Spieler beim aktuellen Ergebnis in Farbe
    if spieler_gesamt_lpz(1, 1, ansetungsmatrix) > 0:
        ttk.Label(frame_topmid, text= "+" + str(spieler_gesamt_lpz(1, 1, ansetungsmatrix)) + "   ", foreground = "green").place(relx=0.90, rely=0.32)
    elif spieler_gesamt_lpz(1, 1, ansetungsmatrix) == 0:
        ttk.Label(frame_topmid, text= "+-" + str(spieler_gesamt_lpz(1, 1, ansetungsmatrix)) + "   ", foreground = "black").place(relx=0.90, rely=0.32)
    else:
        ttk.Label(frame_topmid, text= str(spieler_gesamt_lpz(1, 1, ansetungsmatrix)) + "   ", foreground = "red").place(relx=0.90, rely=0.32)
    
    if spieler_gesamt_lpz(1, 2, ansetungsmatrix) > 0:
        ttk.Label(frame_topmid, text= "+" + str(spieler_gesamt_lpz(1, 2, ansetungsmatrix))+ "   ", foreground = "green").place(relx=0.90, rely=0.43)
    elif spieler_gesamt_lpz(1, 2, ansetungsmatrix) == 0:
        ttk.Label(frame_topmid, text= "+-" + str(spieler_gesamt_lpz(1, 2, ansetungsmatrix))+ "   ", foreground = "black").place(relx=0.90, rely=0.43)
    else:
        ttk.Label(frame_topmid, text= str(spieler_gesamt_lpz(1, 2, ansetungsmatrix))+ "   ", foreground = "red").place(relx=0.90, rely=0.43)
        
    if spieler_gesamt_lpz(1, 3, ansetungsmatrix) > 0:
        ttk.Label(frame_topmid, text="+" +  str(spieler_gesamt_lpz(1, 3, ansetungsmatrix))+ "   ", foreground = "green").place(relx=0.90, rely=0.54)
    elif spieler_gesamt_lpz(1, 3, ansetungsmatrix) == 0:
        ttk.Label(frame_topmid, text= "+-" + str(spieler_gesamt_lpz(1, 3, ansetungsmatrix))+ "     ", foreground = "black").place(relx=0.90, rely=0.54)
    else:
        ttk.Label(frame_topmid, text= str(spieler_gesamt_lpz(1, 3, ansetungsmatrix))+ "     ", foreground = "red").place(relx=0.90, rely=0.54)

    if spieler_gesamt_lpz(2, 1, ansetungsmatrix) > 0:
        ttk.Label(frame_topright, text="+" +  str(spieler_gesamt_lpz(2, 1, ansetungsmatrix)) + "   ", foreground = "green").place(relx=0.90, rely=0.32)
    elif spieler_gesamt_lpz(2, 1, ansetungsmatrix) == 0:
        ttk.Label(frame_topright, text= "+-" + str(spieler_gesamt_lpz(2, 1, ansetungsmatrix)) + "   ", foreground = "black").place(relx=0.90, rely=0.32)
    else:
        ttk.Label(frame_topright, text= str(spieler_gesamt_lpz(2, 1, ansetungsmatrix)) + "   ", foreground = "red").place(relx=0.90, rely=0.32)
    
    if spieler_gesamt_lpz(2, 2, ansetungsmatrix) > 0:
        ttk.Label(frame_topright, text="+" +  str(spieler_gesamt_lpz(2, 2, ansetungsmatrix))+ "   ", foreground = "green").place(relx=0.90, rely=0.43)
    elif spieler_gesamt_lpz(2, 2, ansetungsmatrix) == 0:
        ttk.Label(frame_topright, text= "+-" + str(spieler_gesamt_lpz(2, 2, ansetungsmatrix))+ "   ", foreground = "black").place(relx=0.90, rely=0.43)
    else:
        ttk.Label(frame_topright, text= str(spieler_gesamt_lpz(2, 2, ansetungsmatrix))+ "   ", foreground = "red").place(relx=0.90, rely=0.43)
        
    if spieler_gesamt_lpz(2, 3, ansetungsmatrix) > 0:
        ttk.Label(frame_topright, text="+" +  str(spieler_gesamt_lpz(2, 3, ansetungsmatrix))+ "   ", foreground = "green").place(relx=0.90, rely=0.54)
    elif spieler_gesamt_lpz(2, 3, ansetungsmatrix) == 0:
        ttk.Label(frame_topright, text= "+-" + str(spieler_gesamt_lpz(2, 3, ansetungsmatrix))+ "     ", foreground = "black").place(relx=0.90, rely=0.54)
    else:
        ttk.Label(frame_topright, text= str(spieler_gesamt_lpz(2, 3, ansetungsmatrix))+ "     ", foreground = "red").place(relx=0.90, rely=0.54)
    
    if spielsystem == 4 or spielsystem == 6:
        if spieler_gesamt_lpz(1, 4, ansetungsmatrix) > 0:
            ttk.Label(frame_topmid, text= "+" + str(spieler_gesamt_lpz(1, 4, ansetungsmatrix)) + "   ", foreground = "green").place(relx=0.90, rely=0.65)
        elif spieler_gesamt_lpz(1, 4, ansetungsmatrix) == 0:
            ttk.Label(frame_topmid, text= "+-" + str(spieler_gesamt_lpz(1, 4, ansetungsmatrix)) + "     ", foreground = "black").place(relx=0.90, rely=0.65)
        else:
            ttk.Label(frame_topmid, text= str(spieler_gesamt_lpz(1, 4, ansetungsmatrix)) + "     ", foreground = "red").place(relx=0.90, rely=0.65)
        
        if spieler_gesamt_lpz(2, 4, ansetungsmatrix) > 0:
            ttk.Label(frame_topright, text= "+" + str(spieler_gesamt_lpz(2, 4, ansetungsmatrix)) + "   ", foreground = "green").place(relx=0.90, rely=0.65)
        elif spieler_gesamt_lpz(2, 4, ansetungsmatrix) == 0:
            ttk.Label(frame_topright, text= "+-" + str(spieler_gesamt_lpz(2, 4, ansetungsmatrix)) + "     ", foreground = "black").place(relx=0.90, rely=0.65)
        else:
            ttk.Label(frame_topright, text= str(spieler_gesamt_lpz(2, 4, ansetungsmatrix)) + "     ", foreground = "red").place(relx=0.90, rely=0.65)
            
    if spielsystem == 6:
        if spieler_gesamt_lpz(1, 5, ansetungsmatrix) > 0:
            ttk.Label(frame_topmid, text= "+" + str(spieler_gesamt_lpz(1, 5, ansetungsmatrix)) + "   ", foreground = "green").place(relx=0.90, rely=0.76)
        elif spieler_gesamt_lpz(1, 5, ansetungsmatrix) == 0:
            ttk.Label(frame_topmid, text= "+-" + str(spieler_gesamt_lpz(1, 5, ansetungsmatrix)) + "     ", foreground = "black").place(relx=0.90, rely=0.76)
        else:
            ttk.Label(frame_topmid, text= str(spieler_gesamt_lpz(1, 5, ansetungsmatrix)) + "     ", foreground = "red").place(relx=0.90, rely=0.76)
        
        if spieler_gesamt_lpz(2, 5, ansetungsmatrix) > 0:
            ttk.Label(frame_topright, text= "+" + str(spieler_gesamt_lpz(2, 5, ansetungsmatrix)) + "   ", foreground = "green").place(relx=0.90, rely=0.76)
        elif spieler_gesamt_lpz(2, 5, ansetungsmatrix) == 0:
            ttk.Label(frame_topright, text= "+-" + str(spieler_gesamt_lpz(2, 5, ansetungsmatrix)) + "     ", foreground = "black").place(relx=0.90, rely=0.76)
        else:
            ttk.Label(frame_topright, text= str(spieler_gesamt_lpz(2, 5, ansetungsmatrix)) + "     ", foreground = "red").place(relx=0.90, rely=0.76)
            
        if spieler_gesamt_lpz(1, 6, ansetungsmatrix) > 0:
            ttk.Label(frame_topmid, text= "+" + str(spieler_gesamt_lpz(1, 6, ansetungsmatrix)) + "   ", foreground = "green").place(relx=0.90, rely=0.87)
        elif spieler_gesamt_lpz(1, 6, ansetungsmatrix) == 0:
            ttk.Label(frame_topmid, text= "+-" + str(spieler_gesamt_lpz(1, 6, ansetungsmatrix)) + "     ", foreground = "black").place(relx=0.90, rely=0.87)
        else:
            ttk.Label(frame_topmid, text= str(spieler_gesamt_lpz(1, 6, ansetungsmatrix)) + "     ", foreground = "red").place(relx=0.90, rely=0.87)
        
        if spieler_gesamt_lpz(2, 6, ansetungsmatrix) > 0:
            ttk.Label(frame_topright, text= "+" + str(spieler_gesamt_lpz(2, 6, ansetungsmatrix)) + "   ", foreground = "green").place(relx=0.90, rely=0.87)
        elif spieler_gesamt_lpz(2, 6, ansetungsmatrix) == 0:
            ttk.Label(frame_topright, text= "+-" + str(spieler_gesamt_lpz(2, 6, ansetungsmatrix)) + "     ", foreground = "black").place(relx=0.90, rely=0.87)
        else:
            ttk.Label(frame_topright, text= str(spieler_gesamt_lpz(2, 6, ansetungsmatrix)) + "     ", foreground = "red").place(relx=0.90, rely=0.87)
            

def spieler_gesamt_lpz(team, spielernummer, ansetungsmatrix):
    SummeLPZ = 0
    i=0
    if team == 1:
        for spielernummera, spielernummerb in ansetungsmatrix:
            if spielernummera == spielernummer:
                SummeLPZ = SummeLPZ + spielerteam1_lpz_gesamt[i]
            i = i + 1
    elif team == 2:
        for spielernummera, spielernummerb in ansetungsmatrix:
            if spielernummerb == spielernummer:
                SummeLPZ = SummeLPZ + spielerteam2_lpz_gesamt[i]
            i = i + 1

    return SummeLPZ


def restart_program():
    python = sys.executable
    os.execl(python, python, * sys.argv)


def resource_path(relative_path):
    # zur Erstellung von executables benötigt, um für pyinstaller die relativen pfade von Dateien auslesen zu können.
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)

#update_dropdown_spieler_heim
global root
root = tk.Tk()

# Einsetllungen und Untermenüs des Fensters definieren
version = 1.3

# aktueller Pfad
#path = os.path.abspath(os.path.dirname(__file__))

## Bilder und Icons
# Bild für Ergebniswechsel definieren und skalieren
switchimange = tk.PhotoImage(file = resource_path("change.png"))
switchimange = switchimange.subsample(50, 50)

# Logo in Abhängigkeit vom unterschiedlichen Hintergrund zwischen Linux und Windows anpassen
if platform == "linux" or platform == "linux2":
    logo = tk.PhotoImage(file = resource_path("2019_Logo_TTV-Koenigstein_logo.png"))
elif platform == "win32":
    logo = tk.PhotoImage(file = resource_path("2019_Logo_TTV-Koenigstein_logo_win.png"))
logo = logo.subsample(6, 6) 

# Datei für Settings definieren
settingsfile = ".sttv_lpz_tool.settings"

sttv_mainpage = "https://sttv.tischtennislive.de/default.aspx?L1=Public&L2=AndererVerband"
sttv_inetcheckpage = "sttv.tischtennislive.de"

## allgemeine Fensteroptionen
# Fenstergröße
w_width = 1400
w_high_top = 360
w_high_bottom = 480
w_high = w_high_top + w_high_bottom

# Hintergrundfarbe (Wegen internen Design für Win und Linux unterschiedlich
if platform == "linux" or platform == "linux2":
    color = 'gray85'
elif platform == "win32":
    color = 'gray95'

# Platzierung der Spalten in der Egebnistabelle (relativ)
xlineheim = 0.04
xlinelpz1 = xlineheim + 0.16
xlinelpzdiff1 = xlinelpz1 + 0.07
xlinegame = 0.315
xlineausw = 0.40
xlinelpz2 = xlineausw + 0.16
xlinelpzdiff2 = xlinelpz2 + 0.07
xlineergebnis = 0.70
xlinewkt = 0.81
xlinestand = 0.89
xlinebutton = 0.73

menu = tk.Menu(root)
root.config(menu=menu)
root.title("STTV LPZ- und Spielprognose Tool")
root.resizable(False, False)

if platform == "linux" or platform == "linux2":
    root.iconphoto(False, tk.PhotoImage(file=resource_path("icon/icon.png")))
elif platform == "win32":
    root.iconphoto(False, tk.PhotoImage(file=resource_path("icon.png")))

subMenu = tk.Menu(menu)
menu.add_cascade(label="Datei", menu=subMenu)
subMenu.add_command(label="Neu starten", command=restart_program)
subMenu.add_command(label="Drucken", command=submenus.printer)
subMenu.add_separator()
subMenu.add_command(label="Beenden", command=root.quit)

ueberMenu = tk.Menu(menu)
menu.add_cascade(label="Hilfe", menu=ueberMenu)
ueberMenu.add_command(label="Anleitung", command=lambda:submenus.anleitung(root))
ueberMenu.add_command(label="Über", command=lambda:submenus.ueber(root, version))
ueberMenu.add_separator()
ueberMenu.add_command(label="Auf Update prüfen", command=lambda:submenus.check_for_update(version, root))
ueberMenu.add_separator()
ueberMenu.add_command(label="Lizenshinweise", command=lambda:submenus.lizens(root,resource_path("LICENSE")))


## Fensterinhalte
# Frames
s = ttk.Style()
#s.configure('My.TFrame', background=color)
s.configure('My.TFrame')

frame_topleft = ttk.Frame(master=root, width=w_width/3, height=w_high_top, relief=tk.GROOVE, style='My.TFrame')
frame_topleft.grid(row=0, column=0)

frame_topmid = ttk.Frame(master=root, width=w_width/3, height=w_high_top, relief=tk.GROOVE, style='My.TFrame')
frame_topmid.grid(row=0, column=1)

frame_topright = ttk.Frame(master=root, width=w_width/3, height=w_high_top, relief=tk.GROOVE, style='My.TFrame')
frame_topright.grid(row=0, column=2)

frame_bottom = ttk.Frame(master=root, width=w_width, height=w_high_bottom, relief=tk.FLAT, style='My.TFrame')
frame_bottom.grid(row=1, columnspan = 3)

# Dropdown-Menüs TOP
# Verband
verbandsauswahl = tk.StringVar(frame_topleft)
verbandsauswahl.set("Waehle einen Verband aus...")
verbandsauswahl.trace("w", show_dropdown_liga)
dropdown_verband = ttk.OptionMenu(frame_topleft, verbandsauswahl, "")
dropdown_verband.place(height=30, width=w_width/3.6, relx=0.10, rely=0.08)
dropdown_verband.config(state=tk.DISABLED)

# Liga
ligaauswahl = tk.StringVar(frame_topleft)
ligaauswahl.set("Waehle eine Liga aus...")
ligaauswahl.trace("w", getliganummer)
dropdown_liga = ttk.OptionMenu(frame_topleft, ligaauswahl, "")
dropdown_liga.place(height=30, width=w_width/3.6, relx=0.10, rely=0.21)
dropdown_liga.config(state=tk.DISABLED)

# team1
team1auswahl = tk.StringVar(frame_topmid)
team1auswahl.set("Waehle die Heimmannschaft aus...")
team1auswahl.trace("w", pickteam2)
dropdown_team1 = ttk.OptionMenu(frame_topmid, team1auswahl, "")
dropdown_team1.place(height=30, width=w_width/3.6, relx=0.10, rely=0.08)
dropdown_team1.config(state=tk.DISABLED)

# team2
team2auswahl = tk.StringVar(frame_topright)
team2auswahl.set("Waehle die Gastmannschaft aus...")
team2auswahl.trace("w", read_aufstellung_team1)
dropdown_team2 = ttk.OptionMenu(frame_topright, team2auswahl, "")
dropdown_team2.place(height=30, width=w_width/3.6, relx=0.10, rely=0.08)
dropdown_team2.config(state=tk.DISABLED)

# Spieler
# Spieler Team1
team1spieler1 = tk.StringVar(frame_topmid)
team1spieler1.set("Pos. 1")
dropdown_team1spieler1 = ttk.OptionMenu(frame_topmid, team1spieler1, "")
dropdown_team1spieler1.place(height=30, width=w_width/6.25, relx=0.10, rely=0.30)
dropdown_team1spieler1.config(state=tk.DISABLED)

team1spieler2 = tk.StringVar(frame_topmid)
team1spieler2.set("Pos. 2")
dropdown_team1spieler2 = ttk.OptionMenu(frame_topmid, team1spieler2, "")
dropdown_team1spieler2.place(height=30, width=w_width/6.25, relx=0.10, rely=0.41)
dropdown_team1spieler2.config(state=tk.DISABLED)

team1spieler3 = tk.StringVar(frame_topmid)
team1spieler3.set("Pos. 3")
dropdown_team1spieler3 = ttk.OptionMenu(frame_topmid, team1spieler3, "")
dropdown_team1spieler3.place(height=30, width=w_width/6.25, relx=0.10, rely=0.52)
dropdown_team1spieler3.config(state=tk.DISABLED)

team1spieler4 = tk.StringVar(frame_topmid)
team1spieler4.set("Pos. 4")
dropdown_team1spieler4 = ttk.OptionMenu(frame_topmid, team1spieler4, "")
dropdown_team1spieler4.place(height=30, width=w_width/6.25, relx=0.10, rely=0.63)
dropdown_team1spieler4.config(state=tk.DISABLED)

team1spieler5 = tk.StringVar(frame_topmid)
team1spieler5.set("Pos. 5")
dropdown_team1spieler5 = ttk.OptionMenu(frame_topmid, team1spieler5, "")
dropdown_team1spieler5.place(height=30, width=w_width/6.25, relx=0.10, rely=0.74)
dropdown_team1spieler5.config(state=tk.DISABLED)

team1spieler6 = tk.StringVar(frame_topmid)
team1spieler6.set("Pos. 6")
dropdown_team1spieler6 = ttk.OptionMenu(frame_topmid, team1spieler6, "")
dropdown_team1spieler6.place(height=30, width=w_width/6.25, relx=0.10, rely=0.85)
dropdown_team1spieler6.config(state=tk.DISABLED)

# Spieler Team2
team2spieler1 = tk.StringVar(frame_topright)
team2spieler1.set("Pos. 1")
dropdown_team2spieler1 = ttk.OptionMenu(frame_topright, team2spieler1, "")
dropdown_team2spieler1.place(height=30, width=w_width/6.25, relx=0.10, rely=0.30)
dropdown_team2spieler1.config(state=tk.DISABLED)

team2spieler2 = tk.StringVar(frame_topright)
team2spieler2.set("Pos. 2")
dropdown_team2spieler2 = ttk.OptionMenu(frame_topright, team2spieler2, "")
dropdown_team2spieler2.place(height=30, width=w_width/6.25, relx=0.10, rely=0.41)
dropdown_team2spieler2.config(state=tk.DISABLED)

team2spieler3 = tk.StringVar(frame_topright)
team2spieler3.set("Pos. 3")
dropdown_team2spieler3 = ttk.OptionMenu(frame_topright, team2spieler3, "")
dropdown_team2spieler3.place(height=30, width=w_width/6.25, relx=0.10, rely=0.52)
dropdown_team2spieler3.config(state=tk.DISABLED)

team2spieler4 = tk.StringVar(frame_topright)
team2spieler4.set("Pos. 4")
dropdown_team2spieler4 = ttk.OptionMenu(frame_topright, team2spieler4, "")
dropdown_team2spieler4.place(height=30, width=w_width/6.25, relx=0.10, rely=0.63)
dropdown_team2spieler4.config(state=tk.DISABLED)

team2spieler5 = tk.StringVar(frame_topright)
team2spieler5.set("Pos. 5")
dropdown_team2spieler5 = ttk.OptionMenu(frame_topright, team2spieler5, "")
dropdown_team2spieler5.place(height=30, width=w_width/6.25, relx=0.10, rely=0.74)
dropdown_team2spieler5.config(state=tk.DISABLED)

team2spieler6 = tk.StringVar(frame_topright)
team2spieler6.set("Pos. 6")
dropdown_team2spieler6 = ttk.OptionMenu(frame_topright, team2spieler6, "")
dropdown_team2spieler6.place(height=30, width=w_width/6.25, relx=0.10, rely=0.85)
dropdown_team2spieler6.config(state=tk.DISABLED)

# Label
# Label für LPZ-Punktanzeige neben den Spielern definieren
LABEL_LPZ11 = tk.StringVar()
LABEL_LPZ12 = tk.StringVar()
LABEL_LPZ13 = tk.StringVar()
LABEL_LPZ14 = tk.StringVar()
LABEL_LPZ15 = tk.StringVar()
LABEL_LPZ16 = tk.StringVar()

LABEL_LPZ21 = tk.StringVar()
LABEL_LPZ22 = tk.StringVar()
LABEL_LPZ23 = tk.StringVar()
LABEL_LPZ24 = tk.StringVar()
LABEL_LPZ25 = tk.StringVar()
LABEL_LPZ26 = tk.StringVar()

# Buttons
menubutton1 = ttk.Button(frame_topleft, text="Spielprognose berechnen", command=gamecalc)
menubutton1.place(height=95, width=w_width/7.3, relx=0.10, rely=0.42)
menubutton1.config(state=tk.DISABLED)

menubutton2 = ttk.Button(frame_topleft, text="Neu starten", command=restart_program)
menubutton2.place(height=30, width=w_width/7.3, relx=0.10, rely=0.72)

menubutton3 = ttk.Button(frame_topleft, text="Beenden", command=root.quit)
menubutton3.place(height=30, width=w_width/7.3, relx=0.10, rely=0.85)

# Marker für ersten Durchlauf
run = 0

# Image TTV-Königstein
logocanvas = tk.Canvas(frame_topleft,width=185, height=185, bg=color, highlightthickness=0)
logocanvas.place(x=260, y=155)
logocanvas.create_image(90,90, image = logo)

# Statusbar
status = ttk.Label(root, text=" STTV Spielprognose- und LPZ-Rechner  |  Version " + str(version) + "  |  Manuel Haake", relief=tk.SUNKEN, anchor=tk.W)
status.grid(columnspan=3, sticky=tk.W+tk.E)

# Porgrammstart

# Prüfe, ob falscher Eintrag in settings-file vorhanden ist (bei unvollständiger Übergabe sollte "Waehle Verband oder Waehle Liga" enthalten sein
if os.path.isfile(settingsfile):
    rm = False
    f = open(settingsfile, "r")
    if "Waehle" in f.read():
        rm = True
    f.close()

    if rm:
        os.remove(settingsfile)

# Label für Anweisungen im unteren Frame anzeigen
Anweisung = tk.StringVar()
Anweisungslabel = ttk.Label(frame_bottom, textvariable=Anweisung, state='normal', foreground="grey", font=('Helvetica', 14, 'bold')).place(relx=0.5, rely=0.5, anchor=tk.CENTER)

# Prüfe, ob Internetverbindung besteht. Falls nicht, abbrechen
if not tt_funktionen_gui.pruefe_internetverbindung(sttv_inetcheckpage):
    messagebox.showinfo(title="Achtung", message="Sie haben keine funktionierende Verbindung zum Internet. Ohne diese wird das Programm nicht funktionieren.")
    root.quit

# Erster Funktionsaufruf
if os.path.isfile(settingsfile) and os.access(settingsfile, os.R_OK):
    Anweisung.set("Suche nach Verbänden und Ligen")
    root.update()
    read_and_insert_settings()
else:
    Anweisung.set("Verband auswählen")
    show_dropdown_verbandsauswahl()

root.mainloop()

