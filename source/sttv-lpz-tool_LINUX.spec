# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


added_files = [
         ( 'change.png', '.' ),
         ( '2019_Logo_TTV-Koenigstein_logo.png', '.' ),
         ( 'icon/icon.png', '.' ),
         ( 'LICENSE', '.' )
         ]

a = Analysis(['sttv-lpz-tool.py'],
             pathex=['/media/nucl/network1/home/Dokumente/03_it_krams/scripte_und_co/python/Tischtennis/TT_mit_gui_build'],
             binaries=[],
             hiddenimports=[],
             datas=added_files,
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='sttv-lpz-tool',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True )
