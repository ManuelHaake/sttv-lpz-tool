'''
Diese Datei ist Bestandteil des STTV TT-Tools, 2020 entwickelt von Manuel Haake. Das STTV TT-Tool ist freie Software unter den Bedingungen der GNU General Public License Version 3 (GPL3). Die Haftung ist ausgeschlossen.
Diesem Programm sollte die Datei LICENSE beiliegen, welche die Lizenzbedingungen enthält. Sollte dies nicht der Fall sein, können Sie die Lizenz bei der Free Software Foundation (www.fsf.org) anfordern: Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
Alternativ kann die Lizens unter https://www.gnu.org/licenses/gpl-3.0.html aufgerufen werden

Manuel Haake | 09/2020
'''

import requests
import re
from bs4 import BeautifulSoup
from http.client import HTTPConnection



def waehle_Verband_Links(Hauptseite):
    '''
    Diese Funktion sucht für die HTML "Hauptseite" heraus, welche Verbaende im TT-Live System zur Verfügung stehen
    Diese werden in einer Liste gespeichert und können dann vom Nutzer ausgewaehlt werden...
    '''

    verbandsname = []
    verbandslink = []
    source_code = requests.get(Hauptseite)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, features="html.parser")

    i=0
    for html_verbandeintrag in soup.findAll('a', href=re.compile('tischtennis')):
        if i < 18:
            name = html_verbandeintrag.text
            link = html_verbandeintrag.get('href')
            
            verbandsname.append(name)
            verbandslink.append(link)
            i += 1
    return (verbandslink, verbandsname)


def waehle_Liga_links(verbandslink):
        '''
        Diese Funktion sucht für den in der Funktion "waehle_Verband_Links" heraus, welche im ausgewaehlten Verband im TT-Live System zur Verfügung stehen
        Diese werden in einer Liste gespeichert und können dann vom Nutzer ausgewaehlt werden...
        '''
        liganame = []
        ligalink = []
        source_code = requests.get(verbandslink)
        plain_text = source_code.text
        soup = BeautifulSoup(plain_text, features="html.parser")
        for html_ligaeintraege in soup.findAll('a', href=re.compile('L2P=1')):
            name = html_ligaeintraege.text
            link = verbandslink + html_ligaeintraege.get('href')
            
            # Pokale und "Pokal - Übersichten" als Ligen ausschließen 
            if "pokal" in str(name):
                pass
            else:
                liganame.append(name)
                ligalink.append(link)
        
        if " Sachsenliga" in liganame:
            ligalink, liganame = korrigiere_STTV_Namen(ligalink, liganame)
        
        return (ligalink, liganame)


def korrigiere_STTV_Namen(ligalink, liganame):
        '''
        im Saechsischen Tischtennis-Verband e.V. haben die Ligen im Damen und Herrenbereich gleich Namen (z.B. Sachsenliga). Das Programm liest diese als gleiche
        aus, was spaeter zu fehlern führt. Daher sollen die Suffixe Damen bzw. Herren hinzugefügt werden
        '''
        
        for count,ligen in enumerate(liganame):
            if count<3:
                liganame[count] = liganame[count] + " - Herren"
            else:
                liganame[count] = liganame[count] + " - Damen"
        
        return ligalink, liganame


def waehle_mannschafts_links(ligalink, verbandslink):
    '''
    Diese Funktion sucht für den in der Funktion "waehle_Liga_links" Mannschaften heraus, welche im ausgewaehlten Verband im TT-Live System zur Verfügung stehen
    Diese werden in einer Liste gespeichert und können dann vom Nutzer ausgewaehlt werden...
    '''

    mannschaftsname = []
    mannschaftslink = []

    source_code = requests.get(ligalink)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, features="html.parser")
    for html_mannschaftseintraege in soup.findAll('a', href=re.compile('L3=Mannschaften')):
        name = html_mannschaftseintraege.text
        link = verbandslink + "/" + html_mannschaftseintraege.get('href')
        if name not in mannschaftsname:
            if "//?L1" not in link: # doppelte Einträge mit falschen Links in einigen Manschaftslinks (welche doppelt vorkommen) nicht berücksichtigen
                mannschaftsname.append(name)
                mannschaftslink.append(link)
    
    return (mannschaftslink, mannschaftsname)


def pruefe_ligasystem(ligalink, verbandslink):
    '''
    Diese Funktion prüft, was für ein Spielsystem eine ausgewaehlte Liga hat...
    '''
    source_code3 = requests.get(ligalink)
    plain_text3 = source_code3.text
    soup3 = BeautifulSoup(plain_text3, features="html.parser")
    
    j = 0
    for html_spielsystemeintraege in soup3.findAll('a', href=re.compile('L4=Spielsystem')):
        if j == 0:
            spielsystemlink = verbandslink + "/" + html_spielsystemeintraege.get('href')
            source_code4 = requests.get(spielsystemlink)
            plain_text4 = source_code4.text
            soup4 = BeautifulSoup(plain_text4, features="html.parser")
            

            spielsystem = 0
            if soup4(text=re.compile('3er')) and soup4(text=re.compile('Swaythling')):
                spielsystem = 3
            if soup4(text=re.compile('4er')) and soup4(text=re.compile('Werner')) and soup4(text=re.compile('Scheffler')):
                spielsystem = 4
            if soup4(text=re.compile('6er')) and soup4(text=re.compile('Paarkreuz')):
                spielsystem = 6

            j += 1
            
    return spielsystem


def suche_stammspieler(mannschaftshttp, verbandshttp, out_queue1):
        '''
        Diese Funktion sucht für die angegebene Mannschaften heraus, welche Spieler gemeldet sind.
        Zudem werden die Werte der LPZ in einer Liste gespeichert.
        '''

        spielernummer = []
        spielername = []
        spielerlpz = []
        spielerbemerkung = []
        spielerinaktivitaet = []

        source_code = requests.get(mannschaftshttp)
        plain_text = source_code.text
        soup = BeautifulSoup(plain_text, features="html.parser")

        # Gebe Nullwerte zurück, wenn noch keine Aufstellung freigegeben ist
        if "Die Aufstellungen wurden noch nicht freigegeben" in plain_text:
            out_queue1.put("")
            out_queue1.put("")
            out_queue1.put("")
            out_queue1.put("")
            out_queue1.put("")
            out_queue1.put("")
            return


        tabeigenschaften = {
            "class": "ContentText",
            "cellspacing": "0",
            "border": "0",
            "style": "width:100%",
        }

        tabelle = soup.findAll('table', tabeigenschaften)
        table = tabelle[0]

        for i in range(1, (len(table) - 1)):
            row = table.findAll("tr")[i]
            cells = row.findAll("td")

            bemerkung = cells[1].find(text=True)
            
            if "/" not in bemerkung and "." in bemerkung:  # Wenn ein Spieler Ersatzspiler ist, wird er unter der Nummer mit "Mannschaft/Pos" gelsitet. Ein "/" bedeudet also, dass der Spieler kein Stammspieler ist
                spielernummer.append(i)
                spielername.append(clean_name(cells[3].find(text=True)))
                spielerbemerkung.append(cells[1].find(text=True))

                lpz = cells[-2].findAll(text=True)
                
                # auslesen, ob keine Spiele in den letzten 12 Monaten für den Spieler hinterlegt ist. Dies ist der Fall, wenn die LPZ in eckigen Klammern stehen -> Suchkriterium hierfür ist "'["
                try:
                    if "'[" in str(lpz):
                        spielerinaktivitaet.append(1)
                    elif "Weniger" in str(cells):
                        spielerinaktivitaet.append(2)
                    else:
                        spielerinaktivitaet.append(0)
                except:
                        spielerinaktivitaet.append(0)
                        
                # LPZ-Punktzahl auslesen
                try:
                    lpz3 = (" ".join(re.findall(r"[0-9]*", lpz[1]))).replace("  ", " ")
                except IndexError:
                    try:
                        lpz3 = (" ".join(re.findall(r"[0-9]*", lpz[0]))).replace("  ", " ")
                    except IndexError:
                        #Standardwert, wenn online kein LPZ-Wert vergeben ist
                        lpz3 = "750"

                spielerlpz.append(int(lpz3))

        for vereinsuebersicht in soup.findAll('a', href=re.compile('Page=Spielbetrieb')):
            link = verbandshttp + "/" + vereinsuebersicht.get('href')

        link_vereinsuebersicht = link

        #return (spielernummer, spielername, spielerlpz, spielerbemerkung, link_vereinsuebersicht, spielerinaktivitaet)
        out_queue1.put(spielernummer)
        out_queue1.put(spielername)
        out_queue1.put(spielerlpz)
        out_queue1.put(spielerbemerkung)
        out_queue1.put(link_vereinsuebersicht)
        out_queue1.put(spielerinaktivitaet)


def suche_stammspieler2(mannschaftshttp, verbandshttp):
    '''
    Diese Funktion sucht für die angegebene Mannschaften heraus, welche Spieler gemeldet sind.
    Zudem werden die Werte der LPZ in einer Liste gespeichert.

    ANALOG ZUR OBEREN VERSION, allerdings nicht für Output mit Threads gedacht.
    '''

    spielernummer = []
    spielername = []
    spielerlpz = []
    spielerbemerkung = []
    spielerinaktivitaet = []

    source_code = requests.get(mannschaftshttp)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, features="html.parser")

    tabeigenschaften = {
        "class": "ContentText",
        "cellspacing": "0",
        "border": "0",
        "style": "width:100%",
    }

    tabelle = soup.findAll('table', tabeigenschaften)
    table = tabelle[0]

    for i in range(1, (len(table) - 1)):
        row = table.findAll("tr")[i]
        cells = row.findAll("td")

        bemerkung = cells[1].find(text=True)

        if "/" not in bemerkung and "." in bemerkung:  # Wenn ein Spieler Ersatzspiler ist, wird er unter der Nummer mit "Mannschaft/Pos" gelsitet. Ein "/" bedeudet also, dass der Spieler kein Stammspieler ist
            spielernummer.append(i)
            spielername.append(clean_name(cells[3].find(text=True)))
            spielerbemerkung.append(cells[1].find(text=True))

            lpz = cells[-2].findAll(text=True)

            # auslesen, ob keine Spiele in den letzten 12 Monaten für den Spieler hinterlegt ist. Dies ist der Fall, wenn die LPZ in eckigen Klammern stehen -> Suchkriterium hierfür ist "'["
            try:
                if "'[" in str(lpz):
                    spielerinaktivitaet.append(1)
                elif "Weniger" in str(cells):
                    spielerinaktivitaet.append(2)
                else:
                    spielerinaktivitaet.append(0)
            except:
                spielerinaktivitaet.append(0)

            # LPZ-Punktzahl auslesen
            try:
                lpz3 = (" ".join(re.findall(r"[0-9]*", lpz[1]))).replace("  ", " ")
            except IndexError:
                try:
                    lpz3 = (" ".join(re.findall(r"[0-9]*", lpz[0]))).replace("  ", " ")
                except IndexError:
                    # Standardwert, wenn online kein LPZ-Wert vergeben ist
                    lpz3 = "750"

            spielerlpz.append(int(lpz3))

    for vereinsuebersicht in soup.findAll('a', href=re.compile('Page=Spielbetrieb')):
        link = verbandshttp + "/" + vereinsuebersicht.get('href')

    link_vereinsuebersicht = link

    return (spielernummer, spielername, spielerlpz, spielerbemerkung, link_vereinsuebersicht, spielerinaktivitaet)


def pruefe_sperrvermerk(Mannschaftslink):
    gesperrte_spieler = []

    source_code = requests.get(Mannschaftslink)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, features="html.parser")

    tabeigenschaften = {
        "width": "100%",
        "cellpadding": "0",
        "cellspacing": "0",
        }

    tabelle = soup.findAll('table', tabeigenschaften)
    table = tabelle[-2]

    row = table.findAll("tr")[0]
    cells = row.findAll("td")
    if "Sperrvermerk" in cells[0].find(text=True):   # Prüft, ob die Tabelle für gesperrte Spieler vorhanden ist...
            tabeigenschaften2 = {
                "class": "ContentText",
                "cellpadding": "1",
                "cellspacing": "0",
                "border": "0",
                "style": "width:100%"
            }

            tabelle2 = soup.findAll('table', tabeigenschaften2)
            table2 = tabelle2[-2]

            for i in range(1, (len(table2))):
                row2 = table2.findAll("tr")[i]
                cells2 = row2.findAll("td")
                gesperrte_spieler.append(cells2[1].find(text=True))


def suche_moegliche_ersatzspieler(link_vereinsuebersicht, mannschaftshttp, verbandslink, Liste_Stammspieler, out_queue1):
    mannschaftsnummer = []
    mannschaftsname = []
    mannschaftslink = []

    ersatzspielernummer = []
    ersatzspielername = []
    ersatzspielerlpz = []
    ersatzspielerbemerkung = []
    ersatzspielerinaktivitaet = []
    
    fehlendeAufstellung = []
    
    source_code = requests.get(link_vereinsuebersicht)
    plain_text = source_code.text
    soup = BeautifulSoup(plain_text, features="html.parser")

    tabeigenschaften = {
        "cellspacing": "0",
        "cellpadding": "2",
        }

    tabelle = soup.findAll('table', tabeigenschaften)
    table = tabelle[0]


    for i in range(1, (len(table))):
        row = table.findAll("tr")[i]
        cells = row.findAll("td")
        try:
            mannschaftsnummer.append(i)
            mannschaftsname.append(cells[1].find(text=True))
            mannschaftslink.append(verbandslink + "/" + cells[3].find(href=True).get('href'))
            #print(verbandslink + "/" + cells[3].find(href=True).get('href'))
            #print(cells[1].find(text=True))
        except:
            fehlendeAufstellung.append(cells[1].find(text=True))


    dummynummer = 1
    gesperrte_spieler = pruefe_sperrvermerk(mannschaftshttp)
    match = 0
    
    for link in range(0, (len(mannschaftslink))):
        # Mögliche Ersatzspieler auslesen
        if mannschaftslink[link] == mannschaftshttp:
            match = 1
            continue
            
        elif mannschaftslink[link] != mannschaftshttp and match == 0:
            continue
            
        elif mannschaftslink[link] != mannschaftshttp and match == 1:
            try:
                ersatzspieler_nummer, ersatzspieler_name, ersatzspieler_lpz, ersatzspieler_bemerkung, link_vereinsuebersicht, ersatzspieler_inaktivitaet = suche_stammspieler2(mannschaftslink[link], verbandslink)
                for i in range (0, len(ersatzspieler_nummer)):
                    if gesperrte_spieler is None:
                        if ersatzspieler_name[i] not in ersatzspielername and ersatzspieler_name[i] not in Liste_Stammspieler:      # doppelte Eintraege vermeiden
                            ersatzspielernummer.append(dummynummer)
                            ersatzspielername.append(ersatzspieler_name[i])
                            ersatzspielerlpz.append(ersatzspieler_lpz[i])
                            ersatzspielerbemerkung.append(ersatzspieler_bemerkung[i])
                            ersatzspielerinaktivitaet.append(ersatzspieler_inaktivitaet[i])
                            dummynummer += 1
                            
                    elif gesperrte_spieler not in ersatzspieler_name[i]:
                        if ersatzspieler_name[i] not in ersatzspielername and ersatzspieler_name[i] not in Liste_Stammspieler:      # doppelte Eintraege vermeiden
                            ersatzspielernummer.append(dummynummer)
                            ersatzspielername.append(ersatzspieler_name[i])
                            ersatzspielerlpz.append(ersatzspieler_lpz[i])
                            ersatzspielerbemerkung.append(ersatzspieler_bemerkung[i])
                            ersatzspielerinaktivitaet.append(ersatzspieler_inaktivitaet[i])
                            dummynummer += 1

            except:
                # Falls für eine Mannschaft keine Aufstellung zur Verfügung steht, diese Mannschaft vermerken und die Information später an den Nutzer ausgeben.
                fehlendeAufstellung.append(mannschaftsname[link])

    #return (ersatzspielernummer, ersatzspielername, ersatzspielerlpz, ersatzspielerbemerkung, ersatzspielerinaktivitaet, fehlendeAufstellung)
    out_queue1.put(ersatzspielernummer)
    out_queue1.put(ersatzspielername)
    out_queue1.put(ersatzspielerlpz)
    out_queue1.put(ersatzspielerbemerkung)
    out_queue1.put(ersatzspielerinaktivitaet)
    out_queue1.put(fehlendeAufstellung)


def ansetzungen(spielsystem):
    if spielsystem == 4:
        ansetzungsmatrix = [[1, 2], [2, 1], [3, 4], [4, 3], [1, 1], [2, 2], [3, 3], [4, 4], [3, 1], [1, 3], [2, 4], [4, 2]]
    elif spielsystem == 6:
        ansetzungsmatrix = [[1, 2], [2, 1], [3, 4], [4, 3], [5, 6], [6, 5], [1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6, 6]]
    elif spielsystem == 3:
        ansetzungsmatrix = [[1, 1], [2, 2], [3, 3], [2, 1], [1, 3], [3, 2], [2, 3], [3, 1], [1, 2]]

    return ansetzungsmatrix


def pruefe_internetverbindung(homepage):
    # Prüft Internetverbindung durch HTTP-Request einer Seite
    conn = HTTPConnection(homepage, timeout=5)
    try:
        conn.request("HEAD", "/")
        conn.close()
        return True
    except:
        conn.close()
        return False


def berechne_siegwahrscheinlichkeit(lpz1, lpz2):
    PA = (1 / (1 + 10 ** ((lpz2 - lpz1) / 150))) * 100
    PB = (1 / (1 + 10 ** ((lpz1 - lpz2) / 150))) * 100

    PA = round(PA, 0)
    PB = round(PB, 0)

    if PA < PB:
        return PB
    else:
        return PA


def berechne_lpzdifferenz(lpz1, lpz2, faktoren1, faktoren2, inaktivitaet1, inaktivitaet2):
    # Berücksichtigt die Faktoren für die Änderungskonstante (U16Jahre, U21 Jahre, Inaktiv für 1 Jahre
    # Faktor 0 -> über 21
    # Faktor 1 -> über 21 und 1 Jahr inaktiv
    # Faktor 2 -> unter 21
    # Faktor 3 -> unter 21 und 1 Jahr inaktiv
    # Faktor 4 -> unter 16
    # Faktor 5 -> unter 16 und 1 Jahr inaktiv
    #
    # inaktivität = 1 --> Spieler war 365 Tage nicht aktiv (LPZ-Punktzahl auf sttv-Seite in eckigen Klammern)
    # inaktivität = 2 --> Spieler hat weniger als 30 bewertete Spiele (Stern neben LPZ-Punktzahl auf sttv-Seite)
    
    Grundwert = 16
    Zusatz_unter21 = 4
    Zusatz_unter16 = 4
    Zusatz_inaktiv = 4
    Zusatz_wengeSpiele = 4
    
    switcher = {
        0: Grundwert,
        1: Grundwert + Zusatz_inaktiv,
        2: Grundwert + Zusatz_unter21,
        3: Grundwert + Zusatz_unter21 + Zusatz_inaktiv,
        4: Grundwert + Zusatz_unter21 + Zusatz_unter16,
        5: Grundwert + Zusatz_unter21 + Zusatz_unter16 + Zusatz_inaktiv,
    }

    # Wenn inaktivitätsvermerk übergeben wurde (inaktivitaet1 bzw. inaktivitaet2 == 1), dann den Faktor um 1 erhöhen (siehe Beschreibung Faktoren)
    if inaktivitaet1 == 1:
        faktoren1 += 1
        
    if inaktivitaet2 == 1:
        faktoren2 += 1

    Aenderungskonstante1 = switcher.get(faktoren1, "nothing")
    Aenderungskonstante2 = switcher.get(faktoren2, "nothing")
    
    # Wenn Spieler einer Mannschaft weniger als 30 bewertete Spiele hat, dann +4 auf Grundwert
    if inaktivitaet1 == 2:
        Aenderungskonstante1 = Aenderungskonstante1 + 4
    if inaktivitaet2 == 2:
        Aenderungskonstante2 = Aenderungskonstante2 + 4
    
    PA = 1 / (1 + 10 ** ((lpz2 - lpz1) / 150))
    PB = 1 / (1 + 10 ** ((lpz1 - lpz2) / 150))

    lpzdiff1_1gewinnt = round ((1 - PA)* Aenderungskonstante1)
    lpzdiff1_1verliert = round((0 - PA) * Aenderungskonstante1)
    lpzdiff2_2gewinnt = round ((1 - PB)* Aenderungskonstante2)
    lpzdiff2_2verliert = round((0 - PB) * Aenderungskonstante2)
    
    # Nachwuchsausgleich
    # Tritt ein Spieler unter 18 Jahren bei einer Veranstaltung gegen einen oder mehrere Gegner an, welche ebenfalls durchschnittlich unter 18 Jahren alt sind, dann werden zu dieser Veranstaltung dem TTR Wert des Spielers 2 Punkte hinzugefügt. Dadurch soll ein Ausgleich für die Tatsache geschaffen werden, dass junge Spieler im Laufe der Saison generell staerker werden, was für Erwachsene im allgemeinen nicht gilt.

    return (lpzdiff1_1gewinnt, lpzdiff1_1verliert, lpzdiff2_2gewinnt, lpzdiff2_2verliert)

def clean_name(name):
    # because of the privacy of the plyers the names should be shortened
    # 
    # therefor this function creates short Names except for the last name of a person
    # examples:
    # Max Mustermann -> M. Mustermann
    # John Müller-Wagner -> J. Müller-Wagner 
    # Karl-Heinz Zimmermann -> K.-H. Zimmermann
    subnames = name.split(" ")
    
    shortname = ""
    for nummer,parts in enumerate(subnames):
        if nummer == len(subnames) - 1:
            # dont change last part of the name
            shortname = shortname + " " + parts
        else:
            # shorten names to *.
            # if name contains "-", than shorten the names before and after each "-". For example "Jasmin-Helen" gets "J.-H."
            if "-" in parts:
                combinednames = parts.split("-")
                for combinednummer,combinedparts in enumerate(combinednames):
                    if combinednummer == len(combinednames) - 1:
                        shortname = shortname + combinedparts[0] + "."
                    else:
                        if nummer == 0:
                            shortname = shortname + " " + combinedparts[0] + ".-"
                        else:
                            shortname = shortname + combinedparts[0] + ".-"   
            else:
                if nummer == 0:
                    shortname = shortname + parts[0] + "."
                else:
                    shortname = shortname + " " + parts[0] + "."
    return(shortname)
