"""
Diese Datei ist Bestandteil des STTV TT-Tools, 2020 entwickelt von Manuel Haake. Das STTV TT-Tool ist freie Software unter den Bedingungen der GNU General Public License Version 3 (GPL3). Die Haftung ist ausgeschlossen.
Diesem Programm sollte die Datei LICENSE beiliegen, welche die Lizenzbedingungen enthält. Sollte dies nicht der Fall sein, können Sie die Lizenz bei der Free Software Foundation (www.fsf.org) anfordern: Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
Alternativ kann die Lizens unter https://www.gnu.org/licenses/gpl-3.0.html aufgerufen werden

Manuel Haake | 09/2020
"""

import tkinter as tk
from tkinter import ttk as ttk
from webbrowser import open_new
from sys import platform
import requests


def printer():
    # Druckfunktion später implementieren.
    tk.messagebox.showinfo(title="Achtung", message="Diese Funktion befindet sich noch im Aufbau.\n\nVersuchen Sie es in einer späteren Version noch einmal.")


def ueber(root, version):
        linkgit = "https://gitlab.com/ManuelHaake/sttv-lpz-tool"
        linkttvkoenigstein = "https://www.ttv-koenigstein.de/"
        linkttvkoenigsteinimpressum = "https://www.ttv-koenigstein.de/impressum/"
        kontaktmail = "manu-ttkoenigstein@mailbox.org"
        
        ueberWindow = tk.Toplevel(root)
        ueberWindow.title("Über STTV LPZ- und Spielprognose Tool")
        ueberWindow.resizable(False, False)

        frame = ttk.Frame(master=ueberWindow, width=600, height=570, relief=tk.SUNKEN, style='My.TFrame')
        frame.pack()
        
        Label0 = ttk.Label(frame, text= "\n\nSTTV LPZ- und Spielprognose Tool" + str(version), foreground = "black", font=('Helvetica', 12, 'bold'), justify=tk.CENTER, anchor = tk.CENTER)
        Label2 = ttk.Label(frame, text= "Version " + str(version), foreground = "black", font=('Helvetica', 12, 'bold'), justify=tk.CENTER, anchor = tk.CENTER)
        Label1 = ttk.Label(frame, text= "Entwickelt 2020 von Manuel Haake", foreground = "black", font=('Helvetica', 11), justify=tk.CENTER, anchor = tk.CENTER)
        
        Label3 = ttk.Label(frame, text= "\n\n\nProbleme und Fehler:", foreground = "black", wraplength=450, justify=tk.LEFT, anchor = tk.W, font=('Helvetica', 11, 'bold'))
        Label4 = ttk.Label(frame, text= "Haben Sie Fehler entdeckt, melden Sie diese bitte auf der gitlab-Seite (Link siehe unten) oder per E-Mail an " + kontaktmail + ".", wraplength=450, justify=tk.LEFT, anchor = tk.W)
        Label5 = ttk.Label(frame, text= "\nLizenshinweise:", foreground = "black", wraplength=450, justify=tk.LEFT, anchor = tk.W, font=('Helvetica', 11, 'bold'))
        Label6 = ttk.Label(frame, text= "Das STTV TT-Tool ist freie Software unter den Bedingungen der GNU General Public License Version 3 (GPL3). Die Haftung ist ausgeschlossen.\n\nDer Quellcode samt vollständigen Lizenstext ist auf gitlab einsehbar:", foreground = "black", wraplength=450, justify=tk.LEFT, anchor = tk.W)
        Label7 = ttk.Label(frame, text= "\nSpenden:", foreground = "black", wraplength=450, justify=tk.LEFT, anchor = tk.W, font=('Helvetica', 11, 'bold'))
        Label8 = ttk.Label(frame, text= "Wenn Ihnen das Tool gefällt und Sie sich dafür bedanken wollen, freut sich der TTV Königstein jederzeit über Spenden für unseren Nachwuchsbereich. Mit den Spenden können wir jungen Spielern die Möglichkeit geben, ihrem Sport unter modernen Bedingungen nachgehen zu können. Kontoinformation finden Sie im Impressum der Vereinshomapge:", foreground = "black", wraplength=450, justify=tk.LEFT, anchor = tk.W)
        Label9 = ttk.Label(frame, text= "\n", foreground = "black", wraplength=450, justify=tk.LEFT, anchor = tk.W)
        
        link1 = ttk.Label(frame, text="https://gitlab.com/ManuelHaake/sttv-lpz-tool", foreground="blue", cursor="hand2", justify=tk.LEFT, anchor = tk.W, font=('Helvetica', 11, 'bold'))
        link1.bind("<Button-1>", lambda e: callback(linkgit))
        
        link2 = ttk.Label(frame, text="https://www.ttv-koenigstein.de/impressum/", foreground="blue", cursor="hand2", justify=tk.LEFT, anchor = tk.W, font=('Helvetica', 11, 'bold'))
        link2.bind("<Button-1>", lambda e: callback(linkttvkoenigsteinimpressum))
        
        Label0.pack()
        Label1.pack()
        Label2.pack()
        Label3.pack(fill=tk.X, padx = 10)
        Label4.pack(fill=tk.X, padx = 10)
        Label5.pack(fill=tk.X, padx = 10)
        Label6.pack(fill=tk.X, padx = 10)
        link1.pack(fill=tk.X, padx = 10)
        Label7.pack(fill=tk.X, padx = 10)
        Label8.pack(fill=tk.X, padx = 10)
        link2.pack(fill=tk.X, padx = 10)
        Label9.pack(fill=tk.X, padx = 10)
        
        ueberWindow.resizable(False, False)

def anleitung(root):
    # Wenn ein Update zur Verfügung steht, wird über diese Funktion das Fenster angezeigt, dass den Nutzer hierüber informiert
    linkgit = "https://gitlab.com/ManuelHaake/sttv-lpz-tool"

    anleitungWindow = tk.Toplevel(root)
    anleitungWindow.title("Bedienung des Tools")
    #anleitungWindow.geometry("650x200")
    
    # Unterschiedliche Fenstergrößen, da Unterschiedliche Standardschriften
    if platform == "linux" or platform == "linux2":
        frame = ttk.Frame(master=anleitungWindow, width=800, height=520, relief=tk.SUNKEN, style='My.TFrame')
    elif platform == "win32":
        frame = ttk.Frame(master=anleitungWindow, width=800, height=430, relief=tk.SUNKEN, style='My.TFrame')
    
    frame.pack()
    
    Label1 = ttk.Label(frame, text= "\nBedienung:", foreground = "black", wraplength=400, justify=tk.LEFT, anchor = tk.W, font=('Helvetica', 11, 'bold'))
    Label2 = ttk.Label(frame, text= "Die Bedienung ist recht intuitiv. Zu Beginn müssen der gewünschte Verband, die Liga sowie 2 Mannschaften über die DropDown-Menüs ausgewählt werden. Nachdem die Gastmannschaft ausgewählt wurde, werden automatisch alle theoretisch möglichen Spieler für die jeweilige Mannschaft gesucht. Dieser Vorgang kann einige Zeit (bis 15sec.) in Anspruch nehmen und das Fenster reagiert in dieser Zeit nicht.\n\nNachdem die Spieler für die Heim- und Gastmannschaft ausgewählt wurden, kann mit dem Button \"Spielprognose berechnen\" der wahrscheinliche Spielausgang berechnet werden.\nDie Ergebnisse der Einzel und Doppel können im Anschluss beliebig vom Nutzer angepasst werden, wobei die entsprechenden LPZ Punkteverteilungen und der Spielstand aktualisiert werden.\n\n", foreground = "black", wraplength=700, justify=tk.LEFT, anchor = tk.W)
    Label3 = ttk.Label(frame, text= "Updates:", foreground = "black", wraplength=700, justify=tk.LEFT, anchor = tk.W, font=('Helvetica', 11, 'bold'))
    Label4 = ttk.Label(frame, text= "Unter dem Menüpunkt *Hilfe* -> *Auf Update prüfen* kann nach verfügbaren Updates gesucht werden. Ist ein Update verfügbar, wir auf die entsprechenden Seiten verwiesen, auf denen das Update bezogen werden kann\n\n\nNähere Informationen finden Sie auf der gitlab-Seite des Programms:", foreground = "black", wraplength=700, justify=tk.LEFT, anchor = tk.W)
    Label5 = ttk.Label(frame, text= "\n", foreground = "black", wraplength=400, justify=tk.LEFT, anchor = tk.W)

    link1 = ttk.Label(frame, text=linkgit, foreground="blue", cursor="hand2", justify=tk.LEFT, font=('Helvetica', 11, 'bold'))
    link1.bind("<Button-1>", lambda e: callback(linkgit))

    Label1.pack(fill=tk.X, padx = 10)
    Label2.pack(fill=tk.X, padx = 10)
    Label3.pack(fill=tk.X, padx = 10)
    Label4.pack(fill=tk.X, padx = 10)
    link1.pack(fill=tk.X, padx = 10)
    Label5.pack(fill=tk.X, padx = 10)


def lizens(root,licensefile):
    # Lizenstext der GPLv3 aus Datei anzeigen
    
    licenseWindow = tk.Toplevel(root)
    licenseWindow.title("Lizens")
    licenseWindow.geometry("575x800")
    licenseWindow.resizable(False, False)
    
    scrollbar = tk.Scrollbar(licenseWindow)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
    listbox = tk.Listbox(licenseWindow)
    listbox.place(height=790, width=550, relx=0.01, rely=0.01)
    file = open(licensefile, 'r').readlines()
    for i in file:
        listbox.insert(tk.END, i.replace("\n", ""))
    listbox.config(yscrollcommand=scrollbar.set)
    scrollbar.config(command=listbox.yview)


def check_for_update(actualversion, root):
    # Holt die Information über die aktuellste Verfügbare Version von der Vereinshomepage des TTV-Königstein
    updatelink = "https://www.ttv-koenigstein.de/wp-content/uploads/version.txt"
    
    try:
        source_code = requests.get(updatelink)
        newest_available_version = source_code.text.replace("\n", "")
    except:
        newest_available_version = 1.0

    createUpdateWindow(root, str(actualversion), str(newest_available_version))


def createUpdateWindow(root, actualversion, newest_available_version):
    # Wenn ein Update zur Verfügung steht, wird über diese Funktion das Fenster angezeigt, dass den Nutzer hierüber informiert
    TTV_Homepage = "https://www.ttv-koenigstein.de/dummy"
    gitlab_Homepage = "https://gitlab.com/ManuelHaake/sttv-lpz-tool"
    
    # 2 verschiedene Fenster in Abhängigkeit vom Vorhandensein eines Updates anzeigen
    if float(actualversion) < float(newest_available_version):
        updateWindow = tk.Toplevel(root)
        updateWindow.title("STTV-LPZ Updater")
        
        frame = ttk.Frame(master=updateWindow, width=700, height=170, relief=tk.SUNKEN, style='My.TFrame')
        frame.pack()
        
        Label0 = ttk.Label(frame, text= "\nUpdate verfügbar!\n", foreground = "black", wraplength=450, justify=tk.LEFT, anchor = tk.W, font=('Helvetica', 11, 'bold'))
        Label1 = ttk.Label(frame, text= "Sie nutzen Version " + actualversion + " des Tools, es ist jedoch ein Update auf Version " + newest_available_version + " verfügbar.\n\nDas Update erhalten Sie von der Vereinsseite des TTV-Königstein oder direkt auf gitlab.\n", foreground = "black", wraplength=700, justify=tk.LEFT, anchor = tk.W)
        Label2 = ttk.Label(frame, text= "", foreground = "black", wraplength=700, justify=tk.LEFT, anchor = tk.W)
        Label3 = ttk.Label(frame, text= "", foreground = "black", wraplength=700, justify=tk.LEFT, anchor = tk.W)
        
        link1 = ttk.Label(frame, text=TTV_Homepage, foreground="blue", cursor="hand2", justify=tk.LEFT, font=('Helvetica', 11, 'bold'))
        link1.bind("<Button-1>", lambda e: callback(TTV_Homepage))
        
        link2 = ttk.Label(frame, text=gitlab_Homepage, foreground="blue", cursor="hand2", justify=tk.LEFT, font=('Helvetica', 11, 'bold'))
        link2.bind("<Button-1>", lambda e: callback(gitlab_Homepage))
        
        Label0.pack(fill=tk.X, padx = 10)
        Label1.pack(fill=tk.X, padx = 10)
        link1.pack(fill=tk.X, padx = 10)
        Label2.pack(fill=tk.X, padx = 10)
        link2.pack(fill=tk.X, padx = 10)
        Label3.pack(fill=tk.X, padx = 10)
        
    else:
        updateWindow = tk.Toplevel(root)
        updateWindow.title("STTV-LPZ Updater")
        
        frame = ttk.Frame(master=updateWindow, width=500, height=80, relief=tk.SUNKEN, style='My.TFrame')
        frame.pack()
        
        Label0 = ttk.Label(frame, text= "\nKein Update verfügbar!\n", foreground = "black", wraplength=450, justify=tk.LEFT, anchor = tk.W, font=('Helvetica', 11, 'bold'))
        Label1 = ttk.Label(frame, text= "Sie nutzen mit Version " + actualversion + " des Tools bereits die aktuellste Version.\n", foreground = "black", wraplength=700, justify=tk.LEFT, anchor = tk.W)
        Label0.pack(fill=tk.X, padx = 10)
        Label1.pack(fill=tk.X, padx = 10)

    updateWindow.resizable(False, False)


def callback(url):
    # Link aus Labels heraus öffnen
    open_new(url)
