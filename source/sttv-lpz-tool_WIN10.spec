# -*- mode: python ; coding: utf-8 -*-

block_cipher = None

a = Analysis(['sttv-lpz-tool.py'],
             pathex=['C:\\Users\\claudi\\Documents\\python\\TT_mit_gui\\builds\\build_win10_x64'],
             binaries=[],
             datas=[('2019_Logo_TTV-Koenigstein_logo_win.png', '.'), ('change.png', '.'), ('LICENSE', '.'), ('icon.png', '.')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='sttv-lpz-tool',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False ,
	  icon='icon.ico' )
