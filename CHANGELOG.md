**Version 1.0** (22.09.2020)
 * Erstellung


**Version 1.1** (24.09.2020)
 * Aktualisierung GUI
 * Anpassung GUI-Interface an WIN-Oberfläche
 * Hinzufügen der sys._MEIPASS Funktion, um executables erstellen zu können

 
**Version 1.2** (30.09.2020)
 * UI Anpassungen (Gesamtlpz, Durchschnittlpz, ... )
 * Icon (WIN) hinzugefügt
 * Label für Windows GUI-Ansicht optimiert
 * Markierung durch Stern neben Spieler-LPZ, wenn Spieler < 30 bewertete Spiele
 * Warnung bei nicht unterstütztem Spielsystem -> z.B. "4er Alle gegen Alle" (siehe Readme)
 * Fehler bei Erkennung der Manschaften in einer Liga (Doppelte vorkommen) behoben

**Version 1.3** (05.10.2020)
 * Anleitungen zur notwendigen Auswahl in Fenster anzeigen
 * speichern der Eingabe von Verband und Liga für nächsten Programmaufruf
 * Wartebalken bei Suche nach möglichen Spielern anzeigen
 * Wartebalken bei Programmstart anzeigen, während Ligen und Verbände gesucht werden
 * Fehler/Bug bei tracing der Optionmenüs behoben (sonst unnötige Durchläufe der Funktionen)
 * UI Anpassungen für erhöhte Benutzerfreundlichkeit
 
**Version 1.4** (09.10.2020)
 * Multithread beim Auslesen möglicher Spieler und Ersatzspieler (Zeitersparnis)
 * Optimierung einiger Funktionen bezüglich Geschweindigkeit
 * Menuführung korrigiert (löschen von Spielernamen und LPZ bei Verbands- oder Ligawechsel)
 * Beschleunigung der trace-Befehle aller Auswahlmenüs
 
**Version 1.5** (tba)
 * initiale Prüfung der Internetverbindung optimiert
 * Prüfung der Internetverbindung ohne Nutzung von "google.com"
 * Wechsel des Codes von Github zu Gitlab
