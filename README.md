# LPZ- und Spielprognose-Tool

## Was macht das Tool?
Das LPZ- und Spielprognose-Tool ist ein Programm, mit dessen Hilfe man Punktspiele aus dem Bereich des Sächsischen Tischtennis Verbandes (https://sttv.tischtennislive.de/) auswerten kann. Das Programm ermittelt auf Grundlage von LPZ-Werten einen wahrscheinlichen Spielausgang. Darauf basierend wird für jeden Spieler eine resultierende Gesamt-LPZ-Differenz ermittelt. Über einfache Buttons kann der Benutzer jeden Spielausgang auch festlegen.

## Installation und Bedienung:

### Windows 10 (64-bit)
Die .exe-Datei kann ohne Installation ausgeführt und benutzt werden.

Wer der .exe-Datei nicht vertraut, kann gern den einsehbaren Quellcode selbst ausführen. Hierzu muss zunächst Python (Version > 3.7.3) installiert werden ([Installationsanleitung](https://www.python.org/downloads/windows/)).
Nachdem das Programm heruntergeladen wurde, kann es über den Befehl `python main.py` aus dem entsprechendem Ordner heraus gestartet werden. 

### Linux (64-bit)
Linux-Nutzer können das Programm über die Datei `sttv-lpz-tool_LINUX_x64` auf ihrem System ausführen. Vermutlich muss sie
nach dem Download noch über folgenden Befehl ausführbar gemacht werden:

`chmod +x ./sttv-lpz-tool_LINUX_x64`

Alternativ kann dies bei den gängisten Dateimangern über einen Rechtsklick auf die Datei -> *Eigensachaften* im Reiter *Berechtigungen* eingestellt werden.

**Voraussetungen**
 * **GLIBC in Version 2.28-10 oder höher.** (Welche Version benutzt wird, kann auf den gängisten Systemen über den Befehl `ldd --version` herausgefunden werden.)

### Linux, BSD und MacOS
Um das Programm unter Linux, BSD oder MacOS nutzen zu können, muss es als Python-Code ausgeführt werden.
[Unter pyton.org](https://wiki.python.org/moin/BeginnersGuide/Download) finden Sie die Installationsanleitung für Python unter
Linux und MacOS. Wenn Sie BSD nutzen, werden Sie wissen, wie Python auf ihrem System zu installieren ist.

Nach der Installation von Python kann der Code über den Befehl
```
git clone https://github.com/ManuelHaake/sttv-lpz-tool.git
cd sttv-lpz-tool
python sttv-lpz-tool.py
```
bzw.
```
git clone https://github.com/ManuelHaake/sttv-lpz-tool.git
cd sttv-lpz-tool
python3 sttv-lpz-tool.py
```
ausgeführt werden. Getestet ab **Python-Version 3.7.3**.

### Bedienung
Die Bedienung ist recht intuitiv. Zu Beginn müssen der gewünschte Verband, die Liga sowie 2 Mannschaften über die DropDown-Menüs ausgewählt werden. Nachdem die Gastmannschaft ausgewählt wurde, werden automatisch alle theoretisch möglichen Spieler für die jeweilige Mannschaft gesucht. Dieser Vorgang kann einige Zeit (bis 15sec.) in Anspruch nehmen und das Fenster reagiert in dieser Zeit nicht.
Nachdem die Spieler für die Heim- und Gastmannschaft ausgewählt wurden, kann mit dem Button "Spielprognose berechnen" der wahrscheinliche Spielausgang berechnet werden.

Die Ergebnisse der Einzel und Doppel können im Anschluss beliebig vom Nutzer angepasst werden, wobei die entsprechenden LPZ Punkteverteilungen und der Spielstand aktualisiert werden.

## Updates
Unter dem Menüpunkt *Hilfe* -> *Auf Update prüfen* kann nach verfügbaren updates gesucht werden. Ist ein Update verfügbar, wir auf die entsprechenden Seiten verwiesen, auf denen das Update bezogen werden kann:

  * https://github.com/ManuelHaake/sttv-lpz-tool
  * https://www.ttv-koenigstein.de/

## Einschränkungen und bekannte Fehler

Das Tool unterliegt gewissen Einschränkungen bezüglich der Informationen, die es beziehen kann. Daraus resultieren gewisse Ungenauigkeiten, die dem Nutzer/der Nutzerin bewusst sein müssen. 

### Grundwertberechnung
Ausschlaggebend für die Berechnung der LPZ-Punkte ist die sogenannte Änderungskonstante. Diese hat einen Grundwert von 16 und kann in Abhängigkeit von folgenden Faktoren steigen:

1. Alter des Spielers (Spieler unter 21 Jahre = +4 Punkte für Grundwert / Spieler unter 16 Jahre = +8 Punkte für Grundwert)
2. Der Tatsache, ob der Spieler/ die Spielerin innerhalb der letzten 365 am Spielbetrieb teilnahm. (+4 Punkte für Grundwert)
3. Für einen Spieler bisher weniger als 30 Einzel angerechnet wurden (+4 Punkte für Grundwert)

Das Tool geht bezüglich dieser Punkte folgendermaßen vor:

1. Es ist z.B. nicht möglich, aus den verfügbaren Daten unter https://sttv.tischtennislive.de/ das Alter der Spieler auszulesen, um die Änderungskonstante automatisch entsprechend dem Alter anzupassen. Der/Die Nutzer/in muss daher die Altersangabe (sofern bekannt) selbst vornehmen.
2. Inaktivität wird im auf der Homepage des STTV gekennzeichnet, indem die LPZ-Zahl der betreffenden Person in eckigen Klammern angegeben wird. Dies kann das Tool auslesen.
3. Ob für einen Spieler bisher weniger als 30 Einzel angerechnet wurden, ist über einen Stern in der Aufstellung neben der LPZ-bewertung verzeichnet. Diese Information liest das Tool ebenfalls automatisch aus. 


### Umgang mit bisher unbewerteten Spielern

Es kommt vor, dass neuen Spielern (vor allem aus dem Jugendbereich) noch keine LPZ-Werte zugewiesen wurde. Ist dies der Fall setzt das Tool den LPZ-Wert des entsprechenden Spielers auf 750 fest. Welcher Wert hier richtig wäre, ist mir nicht bekannt. Falls es jemand weiß, würde ich mich über einen entsprechenden Hinweis freuen.

### Unterstützte Spielsysteme
Bisher können nur Ligen ausgewertet werden, die eines der folgenden Spielsysteme nutzen. **"3er Swaythling"**, **"4er Werner Scheffler"** und **"6er Paarkreuz"**.

Die Implementierung des Spielsystems **"4er Jeder gegen Jeden"** ist geplant. Bei Bedarf gern melden, vielleicht schaffe ich es dann eher. 

## Ich habe einen Fehler gefunden - was soll ich tun?
Melde den Fehler gern per Mail an **manu-ttkoenigstein@mailbox.org** oder als Issue auf GitHub.

## Build-Infos

Es liegt die Spezifikationsdatei zum Erstellen der executables via pyinstaller bei. Damit können bei belieben eigene executables gebaut werden.

**Linux**
```
pyinstaller --onefile sttv-lpz-tool_LINUX.spec 
```

**Windows**
```
pyinstaller --onefile --icon D:\source\icon\icon.ico sttv-lpz-tool_WIN10.spec
```

